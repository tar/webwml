#use wml::debian::template title="Debian BTS &mdash; servidor de requisição" NOHEADER=yes NOCOPYRIGHT=true
#use wml::debian::translation-check translation="349225831b38b6f8237943a18d07bb16ba66a2aa"

<h1><a name="introduction">Introdução ao servidor de requisição do sistema de bugs</a></h1>

<p>Existe um servidor de e-mails que pode enviar os relatórios de bugs
e os índices em texto puro, caso requisitados.</p>

<p>Para usá-lo, você deve enviar uma mensagem para <a
href="mailto:request@bugs.debian.org"><code>request@bugs.debian.org</code></a>.
O campo <code>Assunto</code> da mensagem é ignorado, exceto para a
geração do campo <code>Assunto</code> da resposta.</p>

<p>O corpo da mensagem deve ser uma série de comandos, sendo um por linha.
Você receberá uma resposta que se parecerá com uma transcrição
da sua mensagem sendo interpretada, e uma resposta para cada comando.
Para os comandos listados aqui nenhuma notificação é enviada a ninguém, e as
mensagens não são registradas em nenhum lugar disponível publicamente.</p>

<p>Qualquer texto em uma linha iniciando com um sinal <code>#</code>
(cerquilha/hash) é ignorado; o servidor irá parar de processar quando encontrar
uma linha com um <a href="#stopprocessing">terminador de controle</a>
(são exemplos comuns: <code>quit</code>, <code>thank you</code> ou dois hifens).
Ele também irá parar se encontrar muitos comandos não reconhecidos ou mal
formatados. Caso nenhum comando seja interpretado com sucesso, o servidor
enviará um texto de ajuda.</p>

<h1>Comandos disponíveis</h1>

<dl>
<dt><code>send</code> <var>número-do-bug</var></dt>
<dt><code>send-detail</code> <var>número-do-bug</var></dt>
<dd>
Requisita a transcrição do relatório de bug em questão.
<code>send-detail</code> envia também todas as mensagens <q>chatas</q> na
transcrição, como os diversos auto-acks.
</dd>

<dt><code>index</code> [<code>full</code>]</dt>
<dt><code>index-summary by-package</code></dt>
<dt><code>index-summary by-number</code></dt>
<dd>
Requisita o índice completo (com detalhes completos, incluindo relatórios
no estado <q>done</q> e encaminhados), ou o sumário ordenado por
pacote ou por número, respectivamente.
</dd>

<dt><code>index-maint</code></dt>
<dd>
Requisita a página de índice e retorna a lista de mantenedores(as) com bugs
(abertos e fechados recentemente) no sistema de acompanhamento.
</dd>

<dt><code>index maint</code> <var>mantenedor(a)</var></dt>
<dd>
Requisita as páginas de índice dos bugs no sistema para o(a) mantenedor(a)
<var>mantenedor(a)</var>. O termo de procura é uma comparação exata.
O índice do bug será enviado em uma mensagem separada.
</dd>

<dt><code>index-packages</code></dt>
<dd>
Requisita a página de índice e retorna a lista de pacotes com bugs (abertos
e fechados recentemente) no sistema de acompanhamento.
</dd>

<dt><code>index packages</code> <var>pacote</var></dt>
<dd>
Requisita as páginas de índices de bugs no sistema para o pacote
<var>pacote</var>.  O termo de procura é uma comparação exata.
O índice do bug será enviado em uma mensagem separada.
</dd>

<dt><code>getinfo</code> <var>nome-do-arquivo</var></dt>
<dd>
<p>
Requisita um arquivo contendo informações sobre pacote(s) e/ou
mantenedores(as) - os arquivos disponíveis são:
</p>

  <dl>
  <dt><code>maintainers</code></dt>
  <dd>
  A lista unificada dos(as) mantenedores(as) de pacotes, da forma como é usada
  pelo sistema de acompanhamento.
  Tal lista é derivada de informações nos arquivos <code>Packages</code>,
  arquivos override e arquivos de pseudopacotes.
  </dd>

  <dt><code>override.</code><var>versão</var></dt>
  <dt><code>override.</code><var>versão</var><code>.non-free</code></dt>
  <dt><code>override.</code><var>versão</var><code>.contrib</code></dt>
  <dt><code>override.experimental</code></dt>
  <dd>
  Informações sobre as prioridades, seções de pacotes e valores de
  override para mantenedores(as). Essa informação é usada pelo processo que
  gera os arquivos <code>Packages</code> no repositório FTP. As informações
  estão disponíveis para cada uma das principais árvores de versão
  através de suas palavras-código.
  </dd>

  <dt><code>pseudo-packages.description</code></dt>
  <dt><code>pseudo-packages.maintainers</code></dt>
  <dd>
  Lista as descrições e os(as) mantenedores(as), respectivamente, para
  pseudopacotes.
  </dd>
  </dl>
</dd>

<dt><code>refcard</code></dt>
<dd>
Requisita que o cartão de referência do servidor de e-mails seja
enviado em ASCII puro.
</dd>

<dt><a name="user"><code>user</code> <var>endereço</var></a></dt>
<dd>
Define <var>endereço</var> como o novo <q>user</q> de todos os
comandos <code>usertag</code> posteriores.
</dd>

<dt><a name="usertag"><code>usertag</code> <var>número-do-bug</var>
    [ <code>+</code> | <code>-</code> | <code>=</code> ] <var>tag</var>
    [ <var>tag</var> ... ]</a></dt>
<dd>
Permite definir tags por usuário(a). O comando <code>usertag</code>
funciona como o comando <code>tag</code>, com a diferença que
você pode criar sua própria tag. Por padrão, o endereço no cabeçalho do seu
e-mail em <code>From:</code> (De:) ou <code>Reply-To:</code> (Responder-para)
serão usados para definir o(a) usuário(a) de <code>usertag</code>.
</dd>

<dt><a name="usercategory"><code>usercategory</code>
     <var>nome-da-categoria</var> [ <code>[hidden]</code> ]</a></dt>
<dd>
<p>
Adiciona, atualiza ou remove uma <code>usercategory</code>
(categoria-de-usuário). Por padrão, a categoria de usuário(a) é visível. Se o
argumento opcional <code>[hidden]</code> for utilizado, a categoria
de usuário(a) não ficará visível, mas ainda estará disponível para ser
referenciada por outras definições de categoria de usuário(a).
</p>

<p>
Este comando é meio que especial, já que ao adicionar ou atualizar uma categoria
de usuário(a), ele requisitará um corpo de texto imediatamente após o comando.
Se não houver texto, a categoria de usuário(a) será removida. O texto é composto
por linhas começando com qualquer número de espaços. Cada categoria deve começar
com uma linha e com o símbolo <code>*</code> (asterisco), e opcionalmente pode
ser seguida de várias linhas de seleção começando com o símbolo <code>+</code>
(mais). O formato completo ficará assim:
</p>

<div>
* <var>nome-da-categoria-1</var><br />
* <var>Título da categoria 2</var>
  [ <code>[</code><var>prefixo-de-seleção</var><code>]</code> ]<br />
&nbsp;+ <var>Título da seleção 1</var> <code>[</code>
        [ <var>ordem</var><code>:</code> ]
        <var>seleção-1</var> <code>]</code><br />
&nbsp;+ <var>Título da seleção 2</var> <code>[</code>
        [ <var>ordem</var><code>:</code> ]
        <var>seleção-2</var> <code>]</code><br />
&nbsp;+ <var>Título da seleção padrão</var> <code>[</code>
        [ <var>ordem</var>: ] <code>]</code><br />
* <var>nome-da-categoria-3</var><br />
</div>

<p>
Os <var>nomes-da-categoria</var> que aparecem tanto no comando, quanto no corpo
de texto, são usados para se referenciarem, evitando codificação extensa. Os
<var>Títulos da categoria</var> são usados no sumário do relatório de pacote.
</p>

<p>
O parâmetro opcional <var>prefixo-de-seleção</var> é fixado anteriormente para
toda <var>seleção</var> em cada entrada da seção de categorias. Ao que a
primeira <var>seleção</var> é encontrada, o bug é mostrado sob ela. O parâmetro
opcional <var>ordem</var> especifica a posição ao mostrar as entradas
selecionadas, o que pode ser útil quando se encontra uma categoria que seleciona
um grande conjunto de categorias anteriores, mas que precisa ser mostrada antes
delas.
</p>

<p>Os parâmetros <var>nome-da-categoria</var> <code>normal</code> possuem o
significado especial de ser a visualização padrão. Substituindo-os por uma
categoria de usuário(a) diferente para o(a) usuário(a)
<var>nome-do-pacote</var>@packages.debian.org, pode-se alterar a classificação
padrão para um pacote.
</p>

<p>
Exemplo de utilização:
</p>

<pre>
    usercategory dpkg-program [hidden]
     * Program
       + dpkg-deb [tag=dpkg-deb]
       + dpkg-query [tag=dpkg-query]
       + dselect [package=dselect]

    usercategory new-status [hidden]
     * Status [pending=]
       + Outstanding with Patch Available [0:pending+tag=patch]
       + Outstanding and Confirmed [1:pending+tag=confirmed]
       + Outstanding and More Information Needed [pending+tag=moreinfo]
       + Outstanding and Forwarded [pending+tag=forwarded]
       + Outstanding but Will Not Fix [pending+tag=wontfix]
       + Outstanding and Unclassified [2:pending]
       + From other Branch [absent]
       + Pending Upload [pending-fixed]
       + Fixed in NMU [fixed]
       + Resolved [done]
       + Unknown Pending Status []

    \# Change default view
    usercategory normal
      * new-status
      * severity

    usercategory old-normal
      * status
      * severity
      * classification
</pre>
</dd>

<dt><code>help</code></dt>
<dd>
Requisita que este documento de ajuda seja enviado por e-mail em ASCII
puro.
</dd>

<dt><a name="stopprocessing"></a><code>quit</code></dt>
<dt><code>stop</code></dt>
<dt><code>thank</code></dt>
<dt><code>thanks</code></dt>
<dt><code>thankyou</code></dt>
<dt><code>thank you</code></dt>
<dt><code>--</code></dt>
<!-- #366093, I blame you! -->
<!-- <dt><code>kthxbye</code></dt> -->
<!-- See... I documented it! -->
<dd>
Finaliza o processamento neste ponto da mensagem. Depois disso você pode
incluir qualquer texto que desejar e este será ignorado. Você pode usar
esses comandos para incluir comentários mais extensos do que aqueles aplicáveis
para <code>#</code>. Por exemplo, para o benefício das pessoas que estão lendo
sua mensagem (as quais podem ler o texto através dos logs do sistema de
acompanhamento ou devido ao campo <code>Cc</code> ou <code>Cco</code>).
</dd>

<dt><code>#</code>...</dt>
<dd>
Comentário de uma única linha. O <code>#</code> deve estar no início da
linha.
</dd>

<dt><code>debug</code> <var>nível</var></dt>
<dd>
Define o nível de depuração para <var>nível</var>, o qual deve ser um
inteiro não negativo. 0 significa nenhuma depuração; 1 é geralmente
suficiente. A saída da depuração aparece na transcrição. Provavelmente
esta saída não será útil para usuários(as) em geral do sistema de bugs.
</dd>

</dl>

<p>Existe um <a href="server-refcard">cartão de referência</a> para os
servidor de e-mails, disponível via WWW, em
<code>bug-mailserver-refcard.txt</code> ou por e-mail usando o comando
<code>refcard</code> (veja acima).</p>

<p>Caso você deseje manipular os relatórios de bugs, você deverá usar o
endereço <code>control@bugs.debian.org</code>, o qual entende um
<a href="server-control">superconjunto dos comandos listados acima</a>.
Isso é descrito em um outro documento, disponível via
<a href="server-control">WWW</a>, no arquivo
<code>bug-maint-mailcontrol.txt</code>, ou enviando o comando
<code>help</code> para <code>control@bugs.debian.org</code>.</p>

<p>Caso você esteja lendo esse texto como um arquivo em texto puro ou
via e-mail: uma versão HTML está disponível através da página de
conteúdo principal do sistema de bugs
<code>https://www.debian.org/Bugs</code></p>

<hr />

#use "otherpages.inc"

#use "$(ENGLISHDIR)/Bugs/footer.inc"
