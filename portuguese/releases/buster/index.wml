#use wml::debian::template title="Informações de lançamento do Debian &ldquo;buster&rdquo;"
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="014a50a881a72ff5b9866da7ebf1131eb84ce57b"

<p>O Debian <current_release_buster> foi lançado em
<a href="$(HOME)/News/<current_release_newsurl_buster/>"><current_release_date_buster></a>.
<ifneq "10.0" "<current_release>"
  "Debian 10.0 was initially released on <:=spokendate('2019-07-06'):>."
/>
O lançamento incluiu várias grandes mudanças descritas em
nosso <a href="$(HOME)/News/2019/20190706">comunicado à imprensa</a> e
nas <a href="releasenotes">notas de lançamento</a>.</p>

<p><strong>O Debian 10 foi substituído pelo
<a href="../bullseye/">Debian 11 (<q>bullseye</q>)</a>.
As atualizações de segurança foram descontinuadas em <:=spokendate('2022-06-30'):>.
</strong></p>

<p><strong>O Buster também se beneficiou do suporte de longo prazo (Long Term
Support - LTS) até 30 de junho de 2024. O LTS era limitado a i386, amd64,
armel e armhf.
Para mais informações, consulte a
<a href="https://wiki.debian.org/LTS">seção LTS da Wiki do Debian</a>.
</strong></p>

<p>Terceiros oferecem suporte de segurança pago para o buster, até
<:=spokendate('2029-06-30'):>. Veja:
<a href="https://wiki.debian.org/LTS/Extended">suporte LTS extendido</a>.
</p>

<p>Para atualizar a partir de uma versão mais antiga do Debian, veja as
instruções nas
<a href="releasenotes">notas de lançamento</a>.</p>

<p>Arquiteturas suportadas durante o suporte de longo prazo:</p>

<ul>
<li><a href="../../ports/amd64/">64-bit PC (amd64)</a>
<li><a href="../../ports/i386/">32-bit PC (i386)</a>
<li><a href="../../ports/arm64/">64-bit ARM (AArch64)</a>
<li><a href="../../ports/armhf/">Hard Float ABI ARM (armhf)</a>
</ul>

<p>Arquiteturas de computadores suportadas no lançamento inicial do buster:</p>
<ul>
<li><a href="../../ports/amd64/">64-bit PC (amd64)</a>
<li><a href="../../ports/arm64/">64-bit ARM (AArch64)</a>
<li><a href="../../ports/armel/">EABI ARM (armel)</a>
<li><a href="../../ports/armhf/">Hard Float ABI ARM (armhf)</a>
<li><a href="../../ports/i386/">32-bit PC (i386)</a>
<li><a href="../../ports/mips/">MIPS (big endian)</a>
<li><a href="../../ports/mipsel/">MIPS (little endian)</a>
<li><a href="../../ports/mips64el/">64-bit MIPS (little endian)</a>
<li><a href="../../ports/ppc64el/">POWER Processors</a>
<li><a href="../../ports/s390x/">IBM System z</a>
</ul>

<p>Apesar dos nossos desejos, podem existir alguns problemas nesta versão,
embora ela tenha sido declarada <em>estável (stable)</em>. Fizemos
<a href="errata">uma lista dos problemas conhecidos mais importantes</a>,
e você sempre pode relatar outros problemas para nós.</p>
