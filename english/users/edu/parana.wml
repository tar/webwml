# From: Marcos Castilho <marcos@inf.ufpr.br>

<define-tag pagetitle>Departamento de Inform&aacute;tica -- Universidade Federal do Paran&aacute;, Brazil</define-tag>
<define-tag webpage>http://www.inf.ufpr.br/</define-tag>

#use wml::debian::users

<p>
   We use Debian/GNU Linux everywhere and provide a Debian mirror since 1999, 
   and a Debian Security mirror since 2011. Our data center manages packet 
   filtering and routing a 20Gb network, an authoritative DNS server and a mail
   server, all running Debian stable and unstable variants. Using heavily 
   optimized Debian machines, we are able to provide over 40 mirrors for open 
   source projects, being the largest non-commercial mirror in the South 
   Hemisphere and one of the largest of the world.
</p>
<p>
   With Debian we also provide virtualization infrastructure for research 
   projects. We have brute storage of 1PB across multiple distributed file 
   system implementations, over 1700 processing cores and over 10TB of RAM. 
   We manage over 150 virtual machines, all running Debian, providing a variety 
   of services for the community such as document editing collaboration, 
   learning platforms, video calling, version control and scientific databases.
   For bleeding edge research, we also manage a cluster with over 46k CUDA cores
   with Debian. For students, we provide boot over the network facilities, 
   enabling over 370 terminals running Debian and derivatives.
</p>

<p>
   Debian provides us with stability, security and easy maintainability. Our 
   infrastructure is mostly managed by Centro de Computação Científica e 
   Software Livre (C3SL), which is a research group focused on developing open 
   source solutions, being responsible for managing and hosting multiple 
   government projects.
</p>
