#use wml::debian::translation-check translation="5127161ff176cab5734ac40fd7c6f6ccf12526f4"
           <p class="center">
             <a style="margin-left: auto; margin-right: auto;" href="vote_001_results.dot">
               <img src="vote_001_results.png" alt="Grafische weergave van de resultaten">
               </a>
           </p>
             <p>
               In de bovenstaande grafiek duiden alle roze gekleurde knooppunten
               erop dat de optie de meerderheid niet heeft gehaald, het blauwe
               geeft de winnaar aan. Een achthoek wordt gebruikt voor de opties
               die niet boven de standaard uitkwamen.  
           </p>
           <ul>
<li>Option 1 "Jonathan Carter"</li>
<li>Option 2 "Sruthi Chandran"</li>
<li>Option 3 "Brian Gupta"</li>
<li>Option 4 "Geen van bovenstaande"</li>
           </ul>
            <p>
               In de volgende tabel geeft het vak[rij x][kol y] weer hoeveel
               keer optie x verkozen werd boven optie y tijdens de stemming. Een
               <a href="https://en.wikipedia.org/wiki/Schwartz_method">meer
               gedetailleerde uitleg over de vergelijkingsmatrix</a> kan helpen om
               de tabel te begrijpen. Om inzicht te krijgen in de Condorcet-methode,
               is het
               <a href="https://en.wikipedia.org/wiki/Condorcet_method">Wikipedia-artikel</a> redelijk leerzaam.
           </p>
           <table class="vote">
             <caption class="center"><strong>De vergelijkingsmatrix</strong></caption>
	     <tr><th>&nbsp;</th><th colspan="4" class="center">Optie</th></tr>
              <tr>
                   <th>&nbsp;</th>
                   <th>    1 </th>
                   <th>    2 </th>
                   <th>    3 </th>
                   <th>    4 </th>
              </tr>
                 <tr>
                   <th>Optie 1  </th>
                   <td>&nbsp;</td>
                   <td>  258 </td>
                   <td>  281 </td>
                   <td>  301 </td>
                 </tr>
                 <tr>
                   <th>Optie 2  </th>
                   <td>   57 </td>
                   <td>&nbsp;</td>
                   <td>  163 </td>
                   <td>  244 </td>
                 </tr>
                 <tr>
                   <th>Optie 3  </th>
                   <td>   40 </td>
                   <td>  114 </td>
                   <td>&nbsp;</td>
                   <td>  194 </td>
                 </tr>
                 <tr>
                   <th>Optie 4  </th>
                   <td>   34 </td>
                   <td>   76 </td>
                   <td>  125 </td>
                   <td>&nbsp;</td>
                 </tr>
               </table>
              <p>

Bekijken we rij 2, kolom 1, dan blijkt dat Sruthi Chandran<br/>
57 keer verkozen werd boven Jonathan Carter<br/>
<br/>
Bekijken we rij 1, kolom 2, dan blijkt dat Jonathan Carter<br/>
258 keer verkozen werd boven Sruthi Chandran.<br/>
              <h3>Paarsgewijze vergelijking</h3>
              <ul>
                <li>Optie 1 verslaat Optie 2 met ( 258 -   57) =  201 stemmen.</li>
                <li>Optie 1 verslaat Optie 3 met ( 281 -   40) =  241 stemmen.</li>
                <li>Optie 1 verslaat Optie 4 met ( 301 -   34) =  267 stemmen.</li>
                <li>Optie 2 verslaat Optie 3 met ( 163 -  114) =   49 stemmen.</li>
                <li>Optie 2 verslaat Optie 4 met ( 244 -   76) =  168 stemmen.</li>
                <li>Optie 3 verslaat Optie 4 met ( 194 -  125) =   69 stemmen.</li>
              </ul>
              <h3>De Schwartz-verzameling bevat</h3>
              <ul>
                <li>Optie 1 "Jonathan Carter"</li>
              </ul>
              <h3>De winnaars</h3>
              <ul>
                <li>Optie 1 "Jonathan Carter"</li>
              </ul>
              <p>
               Debian gebruikt de Condorcet-methode bij stemmingen.
               Simpel gesteld kan de gewone Condorcet-methode als volgt worden
               weergegeven: <br/>
               <q>Bekijk alle mogelijke wedlopen in twee richtingen tussen
               kandidaten. De Condorcet-winnaar, als er een is, is de kandidaat
               die elke andere kandidaat kan verslaan in een wedloop
               met die kandidaat in de twee richtingen.</q>
               Het probleem is dat er bij complexe verkiezingen wel eens sprake kan
               zijn van een circulaire relatie waarin A B verslaat, B C verslaat en
               C A verslaat. De meeste variaties op Condorcet gebruiken
               verschillende manieren om deze onbesliste stand op te lossen. Zie
               <a href="https://en.wikipedia.org/wiki/Cloneproof_Schwartz_Sequential_Dropping">de methode van het kloonbestendig sequentieel uitrangeren in een Schwartz-verzameling</a>
               voor details. De variatie die Debian gebruikt wordt beschreven in de
               <a href="$(HOME)/devel/constitution">statuten</a>,
               meer bepaald in sectie A.6.
              </p>
