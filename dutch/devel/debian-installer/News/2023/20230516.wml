#use wml::debian::translation-check translation="43bcb91067c2d95af59dbb1b8684ba56b15a7a7a"
<define-tag pagetitle>Release van Debian Installer Bookworm RC 3</define-tag>
<define-tag release_date>2023-05-16</define-tag>
#use wml::debian::news

<p>
Het Debian Installer-<a
href="https://wiki.debian.org/DebianInstaller/Team">team</a> is verheugd de derde releasekandidaat van het installatieprogramma voor Debian 12 <q>Bookworm</q> aan te kondigen.
</p>


<h2>Verbeteringen in deze release</h2>

<ul>
  <li>finish-install:
  <ul>
    <li>Het opschonen van APT-cache aanpassen om te voorkomen dat bash-completion wordt verbroken.
    (<a href="https://bugs.debian.org/1034650">#1034650</a>).</li>
  </ul>
  </li>
  <li>grub-installer:
  <ul>
    <li>EFI-opstartvariabelen met hexadecimale cijfers detecteren, niet alleen met decimale cijfers.</li>
  </ul>
  </li>
  <li>hw-detect:
  <ul>
    <li>Ondersteuning voor firmware-licentieprompts herstellen (<a href="https://bugs.debian.org/1033921">#1033921</a>).</li>
  </ul>
  </li>
  <li>linux:
  <ul>
    <li>Bouwen tegen geüpdatet dwarves, zodat zijn grootte en zijn geheugenvoetafdruk vermindert (<a href="https://bugs.debian.org/1033301">#1033301</a>).</li>
  </ul>
  </li>
  <li>partman-base:
  <ul>
    <li>Ondersteuning toevoegen voor invoer die is ingediend met behulp van macht-van-twee-eenheden: kiB,
    MiB, GiB, etc. (<a href="https://bugs.debian.org/913431">#913431</a>). Houd er rekening mee dat de groottes nog steeds worden weergegeven in machten van tien: kB, MB, GB, etc.</li>
    <li>Ondersteuning toevoegen voor grotere voorvoegsels: petabyte (PB), pebibyte (PiB),
    exabyte (EB), and exbibyte (EiB).</li>
    <li>Met veel dank aan Vincent Danjean!</li>
  </ul>
  </li>
  <li>preconfiguratie:
  <ul>
    <li>Ervoor zorgen dat netcfg rekening houdt met door DHCP verstrekte hostnamen en de hostname-parameter op de kernelopdrachtregel alleen gebruiken als terugvaloptie
    (<a href="https://bugs.debian.org/1035349">#1035349</a>).</li>
  </ul>
  </li>
</ul>


<h2>Veranderingen in de hardwareondersteuning</h2>

<ul>
  <li>debian-installer:
  <ul>
    <li>Specifieke DRM-modules voor bochs en cirrus leveren om defecte graphics onder UEFI/Secure Boot te voorkomen (<a href="https://bugs.debian.org/1036019">#1036019</a>).</li>
  </ul>
  </li>
  <li>linux:
  <ul>
    <li>Zwart scherm op ppc64el omzeilen (<a href="https://bugs.debian.org/1033058">#1033058</a>).</li>
  </ul>
  </li>
  <li>xorg-server:
  <ul>
    <li>Opnieuw modesetting_drv.so leveren in de udeb, waardoor ondersteuning voor het grafische installatiesysteem op UTM hersteld wordt (<a href="https://bugs.debian.org/1035014">#1035014</a>).</li>
  </ul>
  </li>
</ul>


<h2>Toestand van de lokalisatie</h2>

<ul>
  <li>78 talen worden ondersteund in deze release.</li>
  <li>Voor 41 daarvan betreft het een volledige vertaling.</li>
</ul>


<h2>Bekende problemen in deze release</h2>

<p>
Raadpleeg de <a href="$(DEVEL)/debian-installer/errata">errata</a>
voor details en een volledige lijst van bekende problemen.
</p>


<h2>Feedback op deze release</h2>

<p>
We hebben uw hulp nodig om bugs te vinden en het installatiesysteem verder te verbeteren, dus probeer het alstublieft uit. Installatie-cd's, andere media en alles wat u voorts nodig heeft, kunt u vinden op onze <a href="$(DEVEL)/debian-installer">website</a>.
</p>


<h2>Bedanking</h2>

<p>
Het Debian Installer-team bedankt iedereen die bijgedragen heeft aan deze release.
</p>
