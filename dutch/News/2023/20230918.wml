#use wml::debian::translation-check translation="3349f1cfd8d3f3eceed8b216922ec76404aa15f5"
<define-tag pagetitle>DebConf23 in Kochi gesloten en de plaats van DebConf24 aangekondigd</define-tag>
<define-tag release_date>2023-09-18</define-tag>
<define-tag frontpage>yes</define-tag>

#use wml::debian::news

<p>
Gisteren, zondag 17 september 2023, eindigde de jaarlijkse conferentie van ontwikkelaars en medewerkers van Debian.
</p>

<p>
Ruim 474 deelnemers uit 35 landen van over de hele wereld kwamen samen voor een totaal van 89 evenementen bestaande uit lezingen, discussiebijeenkomsten, informele uitwisselingen (Birds of a Feather - BoF), werkgroepen en activiteiten om onze distributie te bevorderen, van onze mentoren en collega's te leren, onze gemeenschap op te bouwen en een beetje plezier te hebben.
</p>

<p>
De conferentie werd voorafgegaan door de jaarlijkse programmeersessie <a href="https://wiki.debian.org/DebCamp">DebCamp</a> van 3 tot en met 9 september, waar Debian-ontwikkelaars en -medewerkers samenkwamen om zich te concentreren op hun individuele projecten in verband met Debian of om in levende lijve samen te werken in teamsprints aan de ontwikkeling van Debian.

In het bijzonder zijn er dit jaar sprints geweest om de ontwikkeling vooruit te helpen van Mobian/Debian, van reproduceerbare compilaties en van Python in Debian. Dit jaar was er ook een BootCamp voor nieuwkomers, georganiseerd door een team van toegewijde mentoren die praktijkervaring met Debian deelden en een dieper inzicht boden in hoe te werken in en bij te dragen aan de gemeenschap.
</p>

<p>
De eigenlijke conferentie van ontwikkelaars van Debian ging van start op zondag 10 september 2023.

Naast activiteiten zoals de traditionele lezing van de projectleider (Bits from the DPL), de permanente ondertekening van sleutels, de blitzlezingen  (lightning talks) en de aankondiging van DebConf24 van volgend jaar, waren er verschillende updatesessies voor interne projecten en teams.

Veel van de gedachtewisselingssessies die plaats vonden, werden geannimeerd door onze technische teams die het werk en de focus belichtten van de teams voor langetermijnondersteuning (Long Term Support - LTS), Android-hulpmiddelen, derivaten van Debian, het installatiesysteem, de installatie-images van Debian en Debian voor wetenschappelijk gebruik. De teams voor de programmeertalen Python, Perl en Ruby deelden ook updates over hun werk en hun doelstellingen.

Twee van de grotere lokale Debian-gemeenschappen, Debian Brazilië en Debian India, deelden hoe hun respectievelijke samenwerkingen in Debian het project vooruit hielpen en hoe ze nieuwe leden aantrokken en nieuwe kansen creëerden voor zowel Debian, F/OSS als de wetenschappelijke wereld met hun HowTo's over aangetoonde betrokkenheid bij de gemeenschap.
</p>

<p>
De <a href="https://debconf23.debconf.org/schedule/">agenda</a> werd dagelijks bijgewerkt met geplande en ad-hocactiviteiten die door deelnemers in de loop van de conferentie werden voorgesteld. Verschillende activiteiten die de afgelopen jaren niet konden plaatsvinden vanwege de wereldwijde COVID-19-pandemie, werden toegejuicht nu ze weer op het programma van de conferentie stonden: een jobbeurs, de open-mic- en poëzieavond, de traditionele kaas- en wijnavond, de groepsfoto's en de daguitstappen.
</p>

<p>
Voor wie er niet bij kon zijn, werden de meeste lezingen en sessies op video opgenomen voor livestreams vanuit de conferentieruimtes. De opgenomen video's zullen later beschikbaar worden gesteld via de <a href="https://meetings-archive.debian.net/pub/debian-meetings/2023/DebConf23/">website voor gearchiveerde Debian-bijeenkomsten</a>.
Bijna voor alle sessies was deelname op afstand mogelijk via IRC-berichtenapps of online collaboratieve tekstdocumenten, waardoor deelnemers vanop afstand 'in de zaal konden zijn' om vragen te stellen of opmerkingen te delen met de spreker of het verzamelde publiek.
</p>

<p>
DebConf23 betekende meer dan 4,3 TiB aan gestreamde data, 55 uur aan geplande voordrachten, 23 netwerktoegangspunten, 11 netwerkswitches, 75 kg aan geïmporteerde apparatuur, 400 meter gebruikte gaffertape, 1.463 uren bekeken streaming, 461 T-shirts, kijkers uit 35 landen volgens IP-geolocatie, 5 daguitstappen en een gemiddelde van 169 voorziene maaltijden per dag.

Al deze evenementen, activiteiten, gesprekken en streams in combinatie met onze liefde, interesse voor en deelname aan Debian en F/OSS hebben deze conferentie zeker tot een groot succes gemaakt, zowel hier in Kochi, India, als online over de hele wereld.
</p>

<p>
De <a href="https://debconf23.debconf.org/">website van DebConf23</a>
blijft actief voor archiefdoeleinden en zal links blijven aanbieden naar de presentaties en video's van lezingen en evenementen.
</p>

<p>
Volgend jaar wordt  <a href="https://wiki.debian.org/DebConf/24">DebConf24</a>
gehouden in Haifa, Israel.
Traditiegetrouw zullen de lokale organisatoren in Israël voorafgaand aan de volgende DebConf de conferentie-activiteiten starten met DebCamp met speciale aandacht voor individueel en teamwerk om de distributie te verbeteren.
</p>

<p>
DebConf streeft naar een veilige en gastvrije omgeving voor alle deelnemers.
Zie de <a href="https://debconf23.debconf.org/about/coc/">webpagina over de gedragscode op de website van DebConf23</a>
voor meer informatie hierover.
</p>

<p>
Debian dankt de talrijke <a href="https://debconf23.debconf.org/sponsors/">sponsors</a>
voor hun engagement bij het ondersteunen van DebConf23, in het bijzonder onze platina sponsors:
<a href="https://www.infomaniak.com">Infomaniak</a>,
<a href="https://www.proxmox.com/">Proxmox</a>,
en <a href="https://www.siemens.com/">Siemens</a>.
</p>

<p>
We willen ook onze video- en infrastructuurteams bedanken, de comités DebConf23 en DebConf, ons gastland India en iedereen die heeft bijgedragen aan dit evenement en aan Debian in het algemeen.

Dank aan iedereen voor het verrichtte werk om Debian te helpen "Het Universele Besturingssysteem" te blijven.

Tot volgend jaar!
</p>

<h2>Over Debian</h2>

<p>
Het Debian-project werd in 1993 door Ian Murdock opgericht als een echt vrij gemeenschapsproject. Sindsdien is het project uitgegroeid tot een van de grootste en meest invloedrijke opensourceprojecten. Duizenden vrijwilligers van over de hele wereld werken samen om Debian-software te maken en te onderhouden. Debian is beschikbaar in 70 talen en ondersteunt een groot aantal computertypes. Het noemt zichzelf het <q>universele besturingssysteem</q>.
</p>

<h2>Overf DebConf</h2>

<p>
DebConf is de conferentie van de ontwikkelaars van het Debian-project. Naast een volledig programma van technische, sociale en beleidslezingen, biedt DebConf een kans voor ontwikkelaars, medewerkers en andere geïnteresseerden om elkaar persoonlijk te ontmoeten en nauwer samen te werken. Het heeft sinds 2000 jaarlijks plaatsgevonden op uiteenlopende locaties als Schotland, Argentinië en Bosnië en Herzegovina. Meer informatie over DebConf is te vinden op
<a href="https://debconf.org/">https://debconf.org</a>.
</p>

<h2>Over Infomaniak</h2>
<p>
<a href="https://www.infomaniak.com">Infomaniak</a> is een belangrijke speler op de Europese cloudmarkt en de toonaangevende ontwikkelaar van webtechnologieën in Zwitserland. Het wil een onafhankelijk Europees alternatief zijn voor de webgiganten en zet zich in voor een ethisch en duurzaam web dat privacy respecteert en lokale banen creëert. Infomaniak ontwikkelt cloud-oplossingen (IaaS, PaaS, VPS), productiviteitstools voor online samenwerking en video- en radiostreamingdiensten.
</p>

<h2>Over Proxmox</h2>
<p>
<a href="https://www.proxmox.com/">Proxmox</a> ontwikkelt krachtige, maar toch makkelijk te gebruiken open-source serversoftware. Het productportfolio van Proxmox, inclusief servervirtualisatie, back-up en e-mailbeveiliging, helpt bedrijven van elke omvang, sector of branche om hun IT-infrastructuur te vereenvoudigen. De oplossingen van Proxmox zijn gebaseerd op het geweldige Debian-platform en we zijn blij dat we iets terug kunnen doen voor de gemeenschap door DebConf23 te sponsoren.
</p>

<h2>Over Siemens</h2>
<p>
<a href="https://www.siemens.com/">Siemens</a> is een technologiebedrijf dat zich richt op industrie, infrastructuur en transport. Van grondstofzuinige fabrieken, veerkrachtige toeleveringsketens, slimmere gebouwen en netwerken tot schoner en comfortabeler transport en geavanceerde gezondheidszorg, creëert het bedrijf doelgerichte technologie die echte waarde toevoegt voor klanten. Door het combineren van de echte en de digitale wereld, stelt Siemens haar klanten in staat om hun industrieën en markten te transformeren en hen te helpen het dagelijks leven van miljarden mensen te verbeteren.
</p>

<h2>Contactinformatie</h2>

<p>Raadpleeg voor verdere informatie de webpagina van DebConf23 op
<a href="https://debconf23.debconf.org/">https://debconf23.debconf.org/</a>
of stuur een e-mail naar &lt;press@debian.org&gt;.</p>
