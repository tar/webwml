#use wml::debian::translation-check translation="f93b89c74962ae015feaa58f013cef8f7ec08dd9"
<define-tag pagetitle>Debian 12 is bijgewerkt: 12.7 werd uitgebracht</define-tag>
<define-tag release_date>2024-08-31</define-tag>
#use wml::debian::news

<define-tag release>12</define-tag>
<define-tag codename>bookworm</define-tag>
<define-tag revision>12.7</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>Het Debian-project kondigt met genoegen de zevende update aan van zijn
stabiele distributie Debian <release> (codenaam <q><codename></q>).
Deze tussenrelease voegt voornamelijk correcties voor beveiligingsproblemen toe,
samen met een paar aanpassingen voor ernstige problemen. Beveiligingsadviezen
werden reeds afzonderlijk gepubliceerd en, waar beschikbaar, wordt hiernaar
verwezen.</p>

<p>Merk op dat de tussenrelease geen nieuwe versie van Debian <release> is,
maar slechts een update van enkele van de meegeleverde pakketten. Het is niet
nodig om oude media met <q><codename></q> weg te gooien. Na de installatie
kunnen pakketten worden opgewaardeerd naar de huidige versie door een
bijgewerkte Debian-spiegelserver te gebruiken.</p>

<p>Wie regelmatig updates installeert vanuit security.debian.org zal niet veel
pakketten moeten updaten, en de meeste van dergelijke updates zijn opgenomen in
de tussenrelease.</p>

<p>Nieuwe installatie-images zullen binnenkort beschikbaar zijn op de gewone
plaatsen.</p>

<p>Het upgraden van een bestaande installatie naar deze revisie kan worden
bereikt door het pakketbeheersysteem naar een van de vele HTTP-spiegelservers
van Debian te verwijzen. Een uitgebreide lijst met spiegelservers is
beschikbaar op:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>

<h2>Secure Boot (veilig opstarten) en andere besturingssystemen</h2>

<p>Gebruikers die andere besturingssystemen opstarten op dezelfde hardware, en die
Secure Boot ingeschakeld hebben, moeten zich ervan bewust zijn dat shim 15.8
(opgenomen in Debian <revision>) handtekeningen in oudere versies van shim in
de UEFI-firmware intrekt. Hierdoor kunnen andere besturingssystemen die een shim
versie gebruiken vóór 15,8, niet opstarten.</p>

<p>Getroffen gebruikers kunnen tijdelijk Secure Boot uitschakelen voordat
andere besturingssystemen worden bijgewerkt.</p>


<h2>Oplossingen voor diverse problemen</h2>

<p>Met deze update van stable, de stabiele distributie, worden een paar
belangrijke correcties aangebracht aan de volgende pakketten:</p>

<table border=0>
<tr><th>Pakket</th>               <th>Reden</th></tr>
<correction amd64-microcode "Nieuwe bovenstroomse release; veiligheidsreparaties [CVE-2023-31315]; reparaties aan SEV firmware [CVE-2023-20584 CVE-2023-31356]">
<correction ansible "Nieuwe bovenstroomse stabiele release; oplossing voor probleem met sleutellek [CVE-2023-4237]">
<correction ansible-core "Nieuwe bovenstroomse stabiele release; oplossen van probleem met openbaarmaking van informatie [CVE-2024-0690]; oplossen van probleem met sjabloon-injectie [CVE-2023-5764]; oplossen van probleem van padoverschrijding [CVE-2023-5115]">
<correction apache2 "Nieuwe bovenstroomse stabiele release; oplossen van probleem met openbaarmaking van informatie [CVE-2024-40725]">
<correction base-files "Update voor de tussenrelease">
<correction cacti "Oplossen van problemen met de uitvoering van externe code [CVE-2024-25641 CVE-2024-31459], van problemen met cross-site scripting [CVE-2024-29894 CVE-2024-31443 CVE-2024-31444], van SQL-injectieproblemen [CVE-2024-31445 CVE-2024-31458 CVE-2024-31460], van een probleem van <q>type juggling</q> [CVE-2024-34340]; autopkgtest-fout oplossen">
<correction calamares-settings-debian "Oplossen van probleem met rechten van Xfce-starter">
<correction calibre "Oplossen van problemen met de uitvoering van externe code [CVE-2024-6782, van probleem met cross-site scripting [CVE-2024-7008], van SQL-injectieprobleem [CVE-2024-7009]">
<correction choose-mirror "Lijst van beschikbare spiegelservers bijwerken">
<correction cockpit "Oplossing voor probleem van denial of service [CVE-2024-6126]">
<correction cups "Problemen met domeinsocketverwerking oplossen [CVE-2024-35235]">
<correction curl "Repareren van probleem met overlezen van de ASN.1 datumontleder [CVE-2024-7264]">
<correction cyrus-imapd "Oplossing voor de in de oplossing voor CVE-2024-34055 geïntroduceerde regressie">
<correction dcm2niix "Mogelijk probleem met code-uitvoering oplossen [CVE-2024-27629]">
<correction debian-installer "Linux kernel ABI verhogen naar 6.1.0-25; herbouwen tegen proposed-updates">
<correction debian-installer-netboot-images "Herbouwen tegen proposed-updates">
<correction dmitry "Beveiligingsoplossingen [CVE-2024-31837 CVE-2020-14931 CVE-2017-7938]">
<correction dropbear "Reparatie voor <q>noremotetcp</q>-gedrag van keepalive-pakketten in combinatie met de restrictie <q>no-port-forwarding</q> authorized_keys(5)">
<correction gettext.js "Oplossen probleem met vervalsing van aanvragen aan de serverkant [CVE-2024-43370]">
<correction glibc "Oplossen van probleem met het vrijgeven van niet-geïnitialiseerd geheugen in libc_freeres_fn(); verschillende prestatieproblemen en mogelijke crashes oplossen">
<correction glogic "Gtk 3.0 en PangoCairo 1.0 vereisen">
<correction graphviz "Fouten bij schalen oplossen">
<correction gtk+2.0 "Zoeken naar modules in de huidige werkmap vermijden [CVE-2024-6655]">
<correction gtk+3.0 "Zoeken naar modules in de huidige werkmap vermijden [CVE-2024-6655]">
<correction imagemagick "Probleem van segmentatiefout oplossen; oplossing voor onvolledige oplossing voor CVE-2023-34151">
<correction initramfs-tools "hook_functions: oplossing voor copy_file als de bron een symbolische koppeling naar een map bevat; hook-functions: copy_file: doelbestandsnaam conform maken; hid-multitouch module installeren voor Surface Pro 4 toetsenbord; toevoegen van hyper-keyboard module, nodig voor de invoer van LUKS-wachtwoord in Hyper-V; auto_add_modules: toevoegen van onboard_usb_hub, onboard_usb_dev">
<correction intel-microcode "Nieuwe bovenstroomse release; beveiligingsoplossingen [CVE-2023-42667 CVE-2023-49141 CVE-2024-24853 CVE-2024-24980 CVE-2024-25939]">
<correction ipmitool "Ontbrekend bestand enterprise-numbers.txt toevoegen">
<correction libapache2-mod-auth-openidc "Een ​​crash voorkomen wanneer de Forwarded-header niet aanwezig is, maar OIDCXForwardedHeaders hiervoor is geconfigureerd">
<correction libnvme "Oplossing voor bufferoverloop tijdens het scannen van apparaten die het lezen van sub-4k niet ondersteunen">
<correction libvirt "birsh: ervoor zorgen dat domif-setlink meer dan eens werkt; qemu: domain: logica herstellen bij domeinbesmetting; problemen met denial of service oplossen [CVE-2023-3750 CVE-2024-1441 CVE-2024-2494 CVE-2024-2496]">
<correction linux "Nieuwe bovenstroomse release; ABI verhogen naar 25">
<correction linux-signed-amd64 "Nieuwe bovenstroomse release; ABI verhogen naar 25">
<correction linux-signed-arm64 "Nieuwe bovenstroomse release; ABI verhogen naar 25">
<correction linux-signed-i386 "Nieuwe bovenstroomse release; ABI verhogen naar 25">
<correction newlib "Bufferoverloopprobleem oplossen [CVE-2021-3420]">
<correction numpy "Conflict met python-numpy">
<correction openssl "Nieuwe bovenstroomse stabiele release; problemen met denial of service oplossen [CVE-2024-2511 CVE-2024-4603]; probleem met gebruik na vrijgave oplossen [CVE-2024-4741]">
<correction poe.app "Commentaarcellen bewerkbaar maken; tekening repareren wanneer een NSActionCell in de voorkeuren wordt opgevolgd om de status te wijzigen">
<correction putty "Oplossing voor het genereren van een te zwak ECDSA nonce waardoor de geheime sleutel kan gevonden worden [CVE-2024-31497]">
<correction qemu "Nieuwe bovenstroomse stabiele release; probleem met denial of service oplossen [CVE-2024-4467]">
<correction riemann-c-client "Voorkomen van misvormde lading in GnuTLS-bewerkingen verzenden/ontvangen">
<correction rustc-web "Nieuwe bovenstroomse stabiele release ter ondersteuning van het bouwen van nieuwe versies van chromium en firefox-esr">
<correction shim "Nieuwe bovenstroomse release">
<correction shim-helpers-amd64-signed "Opnieuw bouwen tegen shim 15.8.1">
<correction shim-helpers-arm64-signed "Opnieuw bouwen tegen shim 15.8.1">
<correction shim-helpers-i386-signed "Opnieuw bouwen tegen shim 15.8.1">
<correction shim-signed "Nieuwe bovenstroomse stabiele release">
<correction systemd "Nieuwe bovenstroomse stabiele release; updaten van hwdb">
<correction usb.ids "Update van de ingesloten gegevenslijst">
<correction xmedcon "Oplossing voor probleem van bufferoverloop [CVE-2024-29421]">
</table>


<h2>Beveiligingsupdates</h2>


<p>Met deze revisie worden de volgende beveiligingsupdates toegevoegd aan de
stabiele release. Het beveiligingsteam heeft voor elk van deze updates
al een advies uitgebracht:</p>

<table border=0>
<tr><th>Advies ID</th>  <th>Pakket</th></tr>
<dsa 2024 5617 chromium>
<dsa 2024 5629 chromium>
<dsa 2024 5634 chromium>
<dsa 2024 5636 chromium>
<dsa 2024 5639 chromium>
<dsa 2024 5648 chromium>
<dsa 2024 5654 chromium>
<dsa 2024 5656 chromium>
<dsa 2024 5668 chromium>
<dsa 2024 5675 chromium>
<dsa 2024 5676 chromium>
<dsa 2024 5683 chromium>
<dsa 2024 5687 chromium>
<dsa 2024 5689 chromium>
<dsa 2024 5694 chromium>
<dsa 2024 5696 chromium>
<dsa 2024 5697 chromium>
<dsa 2024 5701 chromium>
<dsa 2024 5710 chromium>
<dsa 2024 5716 chromium>
<dsa 2024 5719 emacs>
<dsa 2024 5720 chromium>
<dsa 2024 5722 libvpx>
<dsa 2024 5723 plasma-workspace>
<dsa 2024 5724 openssh>
<dsa 2024 5725 znc>
<dsa 2024 5726 krb5>
<dsa 2024 5727 firefox-esr>
<dsa 2024 5728 exim4>
<dsa 2024 5729 apache2>
<dsa 2024 5731 linux-signed-amd64>
<dsa 2024 5731 linux-signed-arm64>
<dsa 2024 5731 linux-signed-i386>
<dsa 2024 5731 linux>
<dsa 2024 5732 chromium>
<dsa 2024 5734 bind9>
<dsa 2024 5735 chromium>
<dsa 2024 5737 libreoffice>
<dsa 2024 5738 openjdk-17>
<dsa 2024 5739 wpa>
<dsa 2024 5740 firefox-esr>
<dsa 2024 5741 chromium>
<dsa 2024 5743 roundcube>
<dsa 2024 5745 postgresql-15>
<dsa 2024 5748 ffmpeg>
<dsa 2024 5749 bubblewrap>
<dsa 2024 5749 flatpak>
<dsa 2024 5750 python-asyncssh>
<dsa 2024 5751 squid>
<dsa 2024 5752 dovecot>
<dsa 2024 5753 aom>
<dsa 2024 5754 cinder>
<dsa 2024 5755 glance>
<dsa 2024 5756 nova>
<dsa 2024 5757 chromium>
</table>


<h2>Verwijderde pakketten</h2>

<p>De volgende pakketten zijn verwijderd door omstandigheden waar wij geen invloed op hadden:</p>

<table border=0>
<tr><th>Pakket</th>               <th>Reden</th></tr>
<correction bcachefs-tools "Vertoont problemen; is verouderd">

</table>

<h2>Het Debian-installatiesysteem</h2>
<p>Het installatiesysteem werd bijgewerkt om de reparaties die met deze tussenrelease in stable, de stabiele release, opgenomen werden, toe te voegen.</p>

<h2>URL's</h2>

<p>De volledige lijsten met pakketten die met deze revisie gewijzigd werden:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>De huidige stabiele distributie:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/stable/">
</div>

<p>Voorgestelde updates voor de stabiele distributie:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/proposed-updates">
</div>

<p>Informatie over de stabiele distributie (notities bij de release, errata, enz.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Beveiligingsaankondigingen en -informatie:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>Over Debian</h2>

<p>Het Debian-project is een samenwerkingsverband van ontwikkelaars van vrije
software die vrijwillig tijd en moeite steken in het produceren van het
volledig vrije besturingssysteem Debian.</p>

<h2>Contactinformatie</h2>

<p>Ga voor verdere informatie naar de webpagina's van Debian op
<a href="$(HOME)/">https://www.debian.org/</a>, stuur een e-mail naar
&lt;press@debian.org&gt;, of neem contact met het release-team voor de stabiele
release op &lt;debian-release@lists.debian.org&gt;.</p>


