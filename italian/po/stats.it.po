# Debian Web Site. stats.it.po
# Francesca Ciceri <madamezou@yahoo.it>
# Luca Monducci <luca.mo@tiscali.it>
# 
msgid ""
msgstr ""
"Project-Id-Version: stats.it.po\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2024-04-08 21:03+0200\n"
"Last-Translator: Luca Monducci <luca.mo@tiscali.it>\n"
"Language-Team: Italian <debian-l10n-italian@lists.debian.org>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../stattrans.pl:282 ../../stattrans.pl:495
msgid "Wrong translation version"
msgstr "Versione della traduzione errata"

#: ../../stattrans.pl:284
msgid "This translation is too out of date"
msgstr "Questa traduzione è troppo datata"

#: ../../stattrans.pl:286
msgid "The original is newer than this translation"
msgstr "L'originale è più recente di questa traduzione"

#: ../../stattrans.pl:290 ../../stattrans.pl:495
msgid "The original no longer exists"
msgstr "L'originale non esiste più"

#: ../../stattrans.pl:474
msgid "hits"
msgstr "visite"

#: ../../stattrans.pl:474
msgid "hit count N/A"
msgstr "numero di visite non disponibile"

#: ../../stattrans.pl:598 ../../stattrans.pl:737
msgid "Created with <transstatslink>"
msgstr "Creato con <transstatslink>"

#: ../../stattrans.pl:603
msgid "Translation summary for"
msgstr "Riepilogo delle traduzioni per"

#: ../../stattrans.pl:606
msgid "Translated"
msgstr "Tradotte"

#: ../../stattrans.pl:606 ../../stattrans.pl:685 ../../stattrans.pl:759
#: ../../stattrans.pl:805 ../../stattrans.pl:848
msgid "Up to date"
msgstr "Aggiornate"

#: ../../stattrans.pl:606 ../../stattrans.pl:760 ../../stattrans.pl:806
msgid "Outdated"
msgstr "Da aggiornare"

#: ../../stattrans.pl:606 ../../stattrans.pl:761 ../../stattrans.pl:807
#: ../../stattrans.pl:850
msgid "Not translated"
msgstr "Non tradotte"

#: ../../stattrans.pl:607 ../../stattrans.pl:608 ../../stattrans.pl:609
#: ../../stattrans.pl:610
msgid "files"
msgstr "file"

#: ../../stattrans.pl:613 ../../stattrans.pl:614 ../../stattrans.pl:615
#: ../../stattrans.pl:616
msgid "bytes"
msgstr "byte"

#: ../../stattrans.pl:623
msgid ""
"Note: the lists of pages are sorted by popularity. Hover over the page name "
"to see the number of hits."
msgstr ""
"Nota: le pagine sono elencate in base alla popolarità. Passare il puntatore "
"del mouse sul nome della pagina per visualizzare il numero di visite."

#: ../../stattrans.pl:629
msgid "Outdated translations"
msgstr "Traduzioni da aggiornare"

#: ../../stattrans.pl:631 ../../stattrans.pl:684
msgid "File"
msgstr "File"

#: ../../stattrans.pl:633
msgid "Diff"
msgstr "Diff"

#: ../../stattrans.pl:635
msgid "Comment"
msgstr "Commento"

#: ../../stattrans.pl:636
msgid "Git command line"
msgstr "Riga di comando git"

#: ../../stattrans.pl:638
msgid "Log"
msgstr "Log"

#: ../../stattrans.pl:639
msgid "Translation"
msgstr "Traduzione"

#: ../../stattrans.pl:640
msgid "Maintainer"
msgstr "Manutentore"

#: ../../stattrans.pl:642
msgid "Status"
msgstr "Stato"

#: ../../stattrans.pl:643
msgid "Translator"
msgstr "Traduttore"

#: ../../stattrans.pl:644
msgid "Date"
msgstr "Data"

#: ../../stattrans.pl:651
msgid "General pages not translated"
msgstr "Pagine generiche non tradotte"

#: ../../stattrans.pl:652
msgid "Untranslated general pages"
msgstr "Pagine generiche non tradotte"

#: ../../stattrans.pl:657
msgid "News items not translated (low priority)"
msgstr "Notizie non tradotte (priorità bassa)"

#: ../../stattrans.pl:658
msgid "Untranslated news items"
msgstr "Notizie non tradotte"

#: ../../stattrans.pl:663
msgid "Consultant/user pages not translated (low priority)"
msgstr "Pagine dei consulenti/utenti non tradotte (priorità bassa)"

#: ../../stattrans.pl:664
msgid "Untranslated consultant/user pages"
msgstr "Pagine dei consulenti e degli utenti non tradotte"

#: ../../stattrans.pl:669
msgid "International pages not translated (very low priority)"
msgstr "Pagine internazionali non tradotte (priorità molto bassa)"

#: ../../stattrans.pl:670
msgid "Untranslated international pages"
msgstr "Pagine internazionali non tradotte"

#: ../../stattrans.pl:675
msgid "Translated pages (up-to-date)"
msgstr "Pagine tradotte (aggiornate)"

#: ../../stattrans.pl:682 ../../stattrans.pl:832
msgid "Translated templates (PO files)"
msgstr "Modelli tradotti (file PO)"

#: ../../stattrans.pl:683 ../../stattrans.pl:835
msgid "PO Translation Statistics"
msgstr "Statistiche di traduzione dei file PO"

#: ../../stattrans.pl:686 ../../stattrans.pl:849
msgid "Fuzzy"
msgstr "Fuzzy"

#: ../../stattrans.pl:687
msgid "Untranslated"
msgstr "Non tradotte"

#: ../../stattrans.pl:688
msgid "Total"
msgstr "Totale"

#: ../../stattrans.pl:705
msgid "Total:"
msgstr "Totale:"

#: ../../stattrans.pl:739
msgid "Translated web pages"
msgstr "Pagine web tradotte"

#: ../../stattrans.pl:742
msgid "Translation Statistics by Page Count"
msgstr "Statistiche di traduzione in base al numero delle pagine"

#: ../../stattrans.pl:757 ../../stattrans.pl:803 ../../stattrans.pl:847
msgid "Language"
msgstr "Lingua"

#: ../../stattrans.pl:758 ../../stattrans.pl:804
msgid "Translations"
msgstr "Traduzioni"

#: ../../stattrans.pl:785
msgid "Translated web pages (by size)"
msgstr "Pagine web tradotte (per dimensione)"

#: ../../stattrans.pl:788
msgid "Translation Statistics by Page Size"
msgstr "Statistiche di traduzione in base alla dimensione della pagina"

#: ../../english/template/debian/stats_tags.wml:6
msgid "Debian web site translation statistics"
msgstr "Statistiche di traduzione per il sito web Debian"

#: ../../english/template/debian/stats_tags.wml:10
msgid "There are %d pages to translate."
msgstr "Ci sono %d pagine da tradurre."

#: ../../english/template/debian/stats_tags.wml:14
msgid "There are %d bytes to translate."
msgstr "Ci sono %d byte da tradurre."

#: ../../english/template/debian/stats_tags.wml:18
msgid "There are %d strings to translate."
msgstr "Ci sono %d stringhe da tradurre."

#~ msgid "Click to fetch diffstat data"
#~ msgstr "Fare clic per recuperare i dati diffstat"

#~ msgid "Colored diff"
#~ msgstr "Diff colorato"

#~| msgid "Colored diff"
#~ msgid "Commit diff"
#~ msgstr "Diff tra commit"

#~ msgid "Created with"
#~ msgstr "Creato con"

#~ msgid "Diffstat"
#~ msgstr "Diffstat"

#~ msgid "Hit data from %s, gathered %s."
#~ msgstr "Numero di visite recuperato da %s il %s."

#~ msgid "Origin"
#~ msgstr "Originale"

#~ msgid "Unified diff"
#~ msgstr "Diff unificato"
