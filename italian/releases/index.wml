#use wml::debian::template title="Versioni di Debian"
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="13e629a7f229b013bb482899912fef0928b23f08" maintainer="Luca Monducci"

<p>Debian contiene sempre almeno tre versioni che sono mantenute in maniera
attiva: <q>stable</q> (stabile), <q>testing</q> (in fase di test) e
<q>unstable</q> (non stabile).</p>

<dl>
<dt><a href="stable/">stable</a></dt>
 <dd>
 <p>
  La distribuzione <q>stable</q> contiene l'ultima versione
  ufficialmente rilasciata di Debian.
 </p>
 <p>
  È quella che deve essere usata per gli ambienti di produzione, quella che
  raccomandiamo di usare.
 </p>
 <p>
  L'attuale distribuzione <q>stable</q> di Debian è la versione
  <:=substr '<current_initial_release>', 0, 2:> ed è chiamata
  <em><current_release_name></em>.
<ifeq "<current_initial_release>" "<current_release>"
  "È stata rilasciata il <current_release_date>."
/>
<ifneq "<current_initial_release>" "<current_release>"
  "La versione iniziale <current_initial_release> è stata rilasciata
  il <current_initial_release_date> e l'ultimo aggiornamento, versione
  <current_release>, è stato rilasciato il <current_release_date>."
/>
 </p>
 </dd>

<dt><a href="testing/">testing</a></dt>
 <dd>
 <p>
  La distribuzione <q>testing</q> contiene i pacchetti che non sono ancora
  stati accettati nella <q>stable</q> ma che sono nella coda per il passaggio.
  Il maggior vantaggio nell'utilizzare questa distribuzione è che include
  versioni più aggiornate del software.
 </p>
 <p>
  Si veda la <a href="$(DOC)/manuals/debian-faq/">Debian FAQ</a> per maggiori informazioni su
  <a href="$(DOC)/manuals/debian-faq/ftparchives#testing">cosa sia <q>testing</q></a>
  e <a href="$(DOC)/manuals/debian-faq/ftparchives#frozen">come diventa <q>stable</q></a>.
 </p>
 <p>
  L'attuale distribuzione <q>testing</q> è chiamata <em><current_testing_name></em>.
 </p>
 </dd>

<dt><a href="unstable/">unstable</a></dt>
 <dd>
 <p>
  La distribuzione <q>unstable</q> è quella in cui si sta portando avanti lo
  sviluppo di Debian. Di norma, questa distribuzione è usata dagli
  sviluppatori che amano avere sempre tutto aggiornato. Si raccomanda agli
  utenti che utilizzano unstable di sottoscrivere la lista di messaggi
  debian-devel-announce per ricevere le notifiche delle principali
  modifiche, per esempio degli aggiornamenti che potrebbero creare dei
  problemi.
 </p>
 <p>
  La versione <q>unstable</q> è sempre chiamata <em>sid</em>.
 </p>
 </dd>
</dl>

<h2>Ciclo di vita dei rilasci</h2>
<p>Debian annuncia il nuovo rilascio stabile a cadenza regolare.
Il ciclo di vita di un rilascio Debian è di cinque anni: i primi 3 anni
con supporto completo seguiti da altri 2 anni con supporto LTS.
</p>

<p>Nelle pagine wiki dei <a
href="https://wiki.debian.org/DebianReleases">Rilasci Debian</a>
e <a href="https://wiki.debian.org/LTS">Debian LTS</a> sono disponibili
informazioni dettagliate.
</p>

<h2>Indice dei rilasci</h2>

<table border="1">
<tr>
  <th>Versione</th>
  <th>Nome in codice</th>
  <th>Data di rilascio</th>
  <th>Data di fine supporto</th>
  <th>Data di fine supporto LTS</th>
  <th>Data di fine supporto ELTS</th>
  <th>Stato</th>
</tr>
<tr>
  <td>14</td>
  <td>Forky</td>
  <td>Da comunicare</td>
  <td>Da comunicare</td>
  <td>Da comunicare</td>
  <td>Da comunicare</td>
  <td>Nome in codice annunciato</td>
</tr>
<tr>
  <td>13</td>
  <td><a href="<current_testing_name>/">Trixie</a></td>
  <td>Da comunicare</td>
  <td>Da comunicare</td>
  <td>Da comunicare</td>
  <td>Da comunicare</td>
  <td>Rilascio <q>testing</q> &mdash; data di rilascio non ancora stabilita</td>
</tr>
<tr>
  <td>12</td>
  <td><a href="bookworm/">Bookworm</a></td>
  <td>2023-06-10</td>
  <td>2026-06-10</td>
  <td>2028-06-30</td>
  <td>2033-06-30</td>
  <td>Rilascio <q>stable</q></td>
</tr>
<tr>
  <td>11</td>
  <td><a href="bullseye/">Bullseye</a></td>
  <td>2021-08-14</td>
  <td>2024-08-14</td>
  <td>2026-08-31</td>
  <td>2031-06-30</td>
  <td>Rilascio <q>oldstable</q></td>
</tr>
<tr>
  <td>10</td>
  <td><a href="buster/">Buster</a></td>
  <td>2019-07-06</td>
  <td>2022-09-10</td>
  <td>2024-06-30</td>
  <td>2029-06-30</td>
  <td>Rilascio archiviato, in <a href="https://wiki.debian.org/LTS/Extended">supporto LTS esteso</a> e a pagamento per terze parti</td>
</tr>
<tr>
  <td>9</td>
  <td><a href="stretch/">Stretch</a></td>
  <td>2017-06-17</td>
  <td>2020-07-18</td>
  <td>2022-07-01</td>
  <td>2027-06-30</td>
  <td>Rilascio archiviato, in <a href="https://wiki.debian.org/LTS/Extended">supporto LTS esteso</a> e a pagamento per terze parti</td>
</tr>
<tr>
  <td>8</td>
  <td><a href="jessie/">Jessie</a></td>
  <td>2015-04-25</td>
  <td>2018-06-17</td>
  <td>2020-06-30</td>
  <td>2025-06-30</td>
  <td>Archived release, under third-party paid <a href="https://wiki.debian.org/LTS/Extended">extended LTS support</a></td>
</tr>
</table>

<p>Rilasci precedenti</p>

<ul>
  <li><a href="wheezy/">Debian 7.0 (<q>wheezy</q>)</a> &mdash; rilascio stabile obsoleto</li>
  <li><a href="squeeze/">Debian 6.0 (<q>squeeze</q>)</a> &mdash; rilascio stabile obsoleto</li>
  <li><a href="lenny/">Debian GNU/Linux 5.0 (<q>lenny</q>)</a> &mdash; rilascio stabile obsoleto</li>
  <li><a href="etch/">Debian GNU/Linux 4.0 (<q>etch</q>)</a> &mdash; rilascio stabile obsoleto</li>
  <li><a href="sarge/">Debian GNU/Linux 3.1 (<q>sarge</q>)</a> &mdash; rilascio stabile obsoleto</li>
  <li><a href="woody/">Debian GNU/Linux 3.0 (<q>woody</q>)</a> &mdash; rilascio stabile obsoleto</li>
  <li><a href="potato/">Debian GNU/Linux 2.2 (<q>potato</q>)</a> &mdash; rilascio stabile obsoleto</li>
  <li><a href="slink/">Debian GNU/Linux 2.1 (<q>slink</q>)</a> &mdash; rilascio stabile obsoleto</li>
  <li><a href="hamm/">Debian GNU/Linux 2.0 (<q>hamm</q>)</a> &mdash; rilascio stabile obsoleto</li>
</ul>

<p>Le pagine web per le vecchie versioni sono mantenute online intatte, ma la
release è mantenuta in un <a href="$(HOME)/distrib/archive">archivio
separato</a>.</p>

<p>Si veda la <a href="$(HOME)/doc/manuals/debian-faq/">Debian FAQ</a> per una spiegazione
dell'<a href="$(HOME)/doc/manuals/debian-faq/ftparchives#sourceforcodenames">origine
dei nomi in codice</a>.</p>

<h2>Integrità dei dati in un rilascio</h2>

<p>L'integrità dei dati è assicurata da un file <code>Release</code> che
viene elettronicamente firmato. Per assicurarsi che tutti i file di un
rilascio vi siano inclusi, viene copiata il checksum dei vari file
<code>Packages</code> all'interno del file <code>Release</code>.</p>

<p>Le firme digitali di questo file sono incluse in un file chiamato
<code>Release.gpg</code>, che usa la versione corrente della chiave
per la firma dell'archivio. Per <q>stable</q> e <q>oldstable</q>
esiste una firma aggiuntiva creata usando una chiave speciale generata
appositamente per il rilascio da un membro dello
<a href="$(HOME)/intro/organization#release-team">Stable Release
Team</a>.</p>
