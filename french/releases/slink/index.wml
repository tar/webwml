#use wml::debian::template title="Informations sur la version Debian 2.1 « Slink »" BARETITLE=yes
#use wml::debian::release
#use wml::debian::translation-check translation="9d55b2307fc18a23f41144b417c9ec941cc3abf8" maintainer="Jean-Paul Guillonneau"

<strong>Debian 2.1 est devenue obsolète.</strong>

<p>

Depuis que des <a href="../">nouvelles versions</a> ont été publiées, la
version 2.1 est devenue obsolète. Ces pages n’existent qu'à titre
historique et vous devez savoir que Debian 2.1 n’est plus entretenue.

<p>

Les architectures suivantes étaient prises en charge dans Debian 2.1 :

<ul>
<li> alpha
<li> i386
<li> mk68k
<li> sparc
</ul>


<h2><a name="release-notes"></a>Notes de publication</h2>

<p>
Pour connaitre les nouveautés dans Debian 2.1, veuillez consulter les notes de
publication pour votre architecture. Celles-ci contiennent des instructions pour
les utilisateurs qui mettent à niveau à partir de publications précédentes.</p>

<ul>
<li> <a href="alpha/release-notes.txt">Notes de publication pour Alpha</a>
<li> <a href="i386/release-notes.txt">Notes de publication pour PC 32 bits (i386)</a>
<li> <a href="m68k/release-notes.txt">Notes de publication pour Motorola 680x0</a>
<li> <a href="sparc/release-notes.txt">Notes de publication pour SPARC</a>
</ul>

<h2><a name="errata"></a>Errata</h2>

<p>

Quelquefois, dans le cas de problèmes sérieux ou de mises à jour de sécurité,
la distribution publiée (dans ce cas, Slink) est mise à jour. Généralement, cela
est signalé comme distribution intermédiaire. La distribution intermédiaire est
actuellement Debian 2.1r5. Vous pouvez trouver le
<a href="http://archive.debian.org/debian/dists/slink/ChangeLog">journal des
modifications</a> dans n’importe quel miroir de l’archive de Debian.

Slink est certifiée pour une utilisation avec la série 2.0.x des noyaux Linux.
Si vous voulez utiliser un noyau Linux de la série 2.2.x avec Slink, consultez
la <a href="running-kernel-2.2">liste des problèmes connus</a>.


<h3>APT</h3>

<p>

Une version d’<code>apt</code> mise à jour est disponible dans Debian, à partir
de la version 2.1r3. L’avantage principal de cette version est qu’elle est
capable de gérer l’installation à partir de plusieurs CD-ROM. Cela rend non
nécessaire l’option d’acquisition <code>dpkg-multicd</code> de
<code>dselect</code>. Cependant, votre CD-ROM 2.1 peut contenir une ancienne
version d’<code>apt</code>, aussi vous pouvez vouloir mettre à niveau vers
cette dernière désormais dans Slink.



<h2><a name="acquiring"></a>Obtenir Debian 2.1</h2>

<p>

Debian est disponible en ligne ou auprès de fournisseurs de CD.

<h3>Achat d’un CD de Debian</h3>

<p>
Nous entretenons une <a href="../../CD/vendors/">liste de fournisseurs de CD</a>
qui vendent des CD de Debian 2.1.


<h3>Téléchargement de Debian depuis Internet</h3>

<p>

Nous entretenons une <a href="../../distrib/ftplist">liste de sites</a> miroirs
de la distribution.


<!-- Keep this comment at the end of the file
Local variables:
mode: sgml
sgml-indent-data:nil
sgml-doctype:"../.doctype"
End:
-->
