#use wml::debian::cdimage title="Imágenes de instalación en vivo"
#use wml::debian::translation-check translation="2059ebe0569b49749803b3394ccae037d12156cb" maintainer="Laura Arjona Reina"
#use wml::debian::release_info
#use wml::debian::installer
#include "$(ENGLISHDIR)/releases/images.data"

<p>Una imagen <q>de instalación en vivo («live»)</q> contiene un sistema Debian que puede iniciarse
sin modificar los ficheros del disco duro y además permite la instalación de Debian
desde los contenidos de esa imagen.
</p>

<p><a name="choose_live"><strong>¿Es adecuada para mí una imagen en vivo?</strong></a> 
Aquí hay algunos aspectos a considerar que le ayudarán a decidir.
<ul>
<li><b>Variantes:</b> La imagen en vivo viene con variantes, proporcionando opciones
para entornos de escritorio (GNOME, KDE, LXDE, Xfce, Cinammon y MATE). 
<li><b>Arquitectura:</b>Actualmente se proporcionan imágenes sólo para PC de 64 bits (amd64).
<li><b>Instalador:</b> Las imágenes en vivo contienen
el <a href="https://calamares.io">instalador Calamares</a>,
un marco de instalación independiente de la distribución.
<li><b>Idiomas:</b> Las imágenes no contienen el conjunto completo de soporte de idiomas. 
Si necesita métodos de entrada, tipos de letra y paquetes adicionales para su idioma, necesitará
instalarlos más tarde.
</ul>


<h2 id="live-install-stable">Imágenes oficiales de instalación en vivo para la distribución <q>estable</q></h2>

<p>Estas imágenes, que se ofrecen en distintas variantes,
son adecuadas para probar un sistema Debian con un conjunto de paquetes seleccionado de manera predeterminada,
y después, instalarlo desde el mismo medio.</p>

<div class="line">
<div class="item col50">
<p><strong>DVD/USB (vía <a
href="$(HOME)/CD/torrent-cd">BitTorrent</a>)</strong></p>
<p>Imagen ISO <q>híbrida</q> adecuada para escribirse en medios DVD-R(W) y también dispositivos USB.</p>
	  <stable-live-install-bt-cd-images />
</div>

<div class="item col50 lastcol"> <p><strong>DVD/USB</strong></p>
<p>Ficheros de imagen ISO <q>híbridas</q> adecuadas para escribir en medios
DVD-R(W), y también dispositivos USB.</p>
       <stable-live-install-iso-cd-images /> 
</div> </div>

<p>Para más información sobre qué son estos ficheros y cómo usarlos, por favor vea
las <a href="../faq/">Preguntas Frecuentes</a>.</p>

<p>Vea la <a href="$(HOME)/devel/debian-live">página del proyecto Debian Live</a> para
más información sobre los sistemas Debian Live proporcionados por estas imágenes.</p>
