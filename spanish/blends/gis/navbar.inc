#use wml::debian::blends::gis
#use wml::debian::translation-check translation="794fc406da417697ec055ef6c78e0cff7510f391"


{#alternate_navbar#:
   <div id="second-nav">
      <p><a href="$(HOME)/blends/gis/">Mezcla&nbsp;Debian&nbsp;GIS</a></p>
    <ul>
      <li><a href="$(HOME)/blends/gis/about">Sobre esta mezcla</a></li>
      <li><a href="$(HOME)/blends/gis/contact">Contacto</a></li>
      <li><a href="$(HOME)/blends/gis/get/">Obtener&nbsp;the&nbsp;Blend</a>
         <ul>
         <li><a href="$(HOME)/blends/gis/get/metapackages">Usar&nbsp;metapaquetes</a></li>
	 </ul>
      </li>
      <li><a href="$(HOME)/blends/gis/deriv">Derivados</a></li>
      <li><a href="https://wiki.debian.org/DebianGis">Desarrollo</a>
        <ul>
        <li><a href="<gis-policy-html/>">Normativa del equipo de GIS</a></li>
        </ul>
      </li>
    </ul>
   </div>
:#alternate_navbar#}
