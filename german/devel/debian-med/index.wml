#use wml::debian::template title="Debian Med"
#use wml::debian::recent_list
#use wml::debian::translation-check translation="b9967d1f4d930716e9646a92bda776ced3c44cce"
# $Id$
# Updated: Holger Wansing <linux@wansing-online.de>, 2011, 2012, 2018.


<h2>Projektbeschreibung</h2>

<p>Debian Med ist ein
   <q><a href="https://blends.debian.org/blends/">Debian Pure Blend</a></q>,
   das das Ziel hat, Debian zu einem Betriebssystem zu entwickeln, das sich
   besonders gut für die Aufgaben der medizinischen Praxis und biomedizinischen
   Forschung eignet.
   Das Ziel von Debian Med ist ein vollständig freies und offenes System
   für alle denkbaren Aufgaben des Gesundheitswesens und der Forschung.
   Um dieses Ziel zu erreichen, integriert Debian Med passende freie und
   quelloffene Software für medizinische Bildverarbeitung, Bioinformatik,
   IT-Infrastruktur in Kliniken und weiteres im Debian-Betriebssystem.
</p>

<p>Debian Med beinhaltet eine Reihe von so genannten Metapaketen, die
   Abhängigkeiten zu Debian-Paketen definieren und so das gesamte System
   zur Lösung bestimmter Aufgaben vorbereiten.
   Den besten Überblick über Debian Med bietet die
   <a href="https://blends.debian.org/med/tasks/">Aufgabenseite</a>.
</p>

<p>Für eine detailliertere Diskussion stehen
   <a href="https://people.debian.org/~tille/talks/index_de.html">verschiedene
   Vorträge über Debian Med und Debian Pure Blends im Allgemeinen</a>
   &ndash; teilweise auch als Video &ndash; zur Verfügung.
</p>

<h2>Kontakt und Entwicklerinformationen</h2>

<p>
Die <a href="mailto:debian-med@lists.debian.org">Debian-Med-Mailingliste</a>
ist die zentrale Kommunikationsstelle von Debian Med. Sie dient als
Forum für potenzielle und bereits aktive Debian-Anwender, die
ihren Rechner für medizinische Zwecke oder Aufgaben einsetzen möchten.
Außerdem wird sie verwendet, um Entwicklungen rund um die verschiedenen
Gebiete der Medizin zu koordinieren. Sie können sie auf der
<a href="https://lists.debian.org/debian-med/">Webseite der
Liste</a> abonnieren und abbestellen. Weiterhin finden Sie auf dieser Seite
das Listenarchiv.
</p>
<p>
   Wichtige Orte für Entwicklerinformationen:
</p>

<ol>
  <li><a href="https://blends.debian.org/med/">Die Blends-Seite</a></li>
  <li>Die <a href="https://med-team.pages.debian.net/policy/">Debian-Med-Policy</a>, hier sind für das Team geltende Paketierungs-Regeln beschrieben</li>
  <li><a href="https://salsa.debian.org/med-team/">GIT-Repositories von Debian-Med-Paketen auf Salsa</a></li>
</ol>


<h2>Eingebundene Softwareprojekte</h2>

<p>
Das System der <a href="https://blends.debian.org/blends">Debian Pure Blends</a>
stellt eine automatisch erzeugte Übersicht aller im Blend enthaltenen Software
zur Verfügung. Die sogenannten 
<b><a href="https://blends.debian.org/med/tasks/">Aufgabenseiten von
Debian Med</a></b> vermitteln einen guten Überblick über alle enthaltene
Software und weitere Projekte, die auf unserer TODO-Liste für die Integration
in Debian stehen.
</p>

<h2>Projektziele</h2>

<ul>
  <li>Aufbau einer soliden Softwarebasis für das
      Gesundheitswesen mit Schwerpunkten auf einfacher
      Installation, einfacher Systempflege und Sicherheit.</li>
  <li>Ermunterung zur Zusammenarbeit von Autoren verschiedener
      Softwareprojekte mit ähnlichen Zielen.</li>
  <li>Testumgebung zur Vereinfachung der Beurteilung der Qualität von
      medizinischer Software.</li>
  <li>Bereitstellung von Informationen über und Dokumentation zu
      freier medizinischer Software.</li>
  <li>Unterstützung der Autoren freier Software beim Paketieren ihrer
      Produkte als Debian-Pakete.</li>
  <li>Demonstration der Stärken eines stabilen Basissystems für
      kommerzielle Softwarefirmen, um sie zur Portierung ihrer
      Software nach Linux zu bewegen oder sie zur Freigabe ihrer
      Software anzuregen.</li>
</ul>


<h2>Wie kann ich helfen?</h2>

<p>
   Es gibt eine <a href="https://wiki.debian.org/DebianMedTodo">Wiki-Seite</a>,
   die eine Liste von Dingen beinhaltet, wie dem Projekt am besten geholfen
   werden kann.
</p>
      

<h2>Vermarktung &amp; Öffentlichkeitsarbeit</h2>

<p>Sobald Debian Med erste Ergebnisse vorzuweisen hat und sogar im
   Anfangsstadium wird das Projekt mit Interesse aus aller Welt
   beobachtet. Daraus ergibt sich die Notwendigkeit der engen
   Zusammenarbeit mit press@debian.org, um das Projekt entsprechend
   bekannt zu machen. Um das zu unterstützen, wird eine Sammlung von
   Präsentationen über Debian Med angelegt werden.
</p>


<h2>Links</h2>

<ul>
  <li>Debian Med arbeitet eng mit
     <a href="http://nebc.nerc.ac.uk/tools/bio-linux/bio-linux-6.0">Bio-Linux</a>
     zusammen. Während Bio-Linux auf Ubuntu-LTS-Veröffentlichungen basiert,
     werden die Pakete aus dem biologischen Bereich innerhalb von Debian durch
     das Debian-Med-Team betreut.</li>
</ul>
