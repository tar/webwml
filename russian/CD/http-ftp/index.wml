#use wml::debian::cdimage title="Загрузка образов CD/DVD дисков Debian через HTTP/FTP" BARETITLE=true
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/images.data"
#use wml::debian::translation-check translation="0ff5192e787beb3cba84667527045bb299e2b1b2" maintainer="Lev Lamberov"

<div class="tip">
<p><strong>Пожалуйста, не загружайте образы CD или DVD из web-браузера тем же
способом, каким вы загружаете остальные файлы!</strong> Это связано с тем,
что после обрыва связи большинство браузеров не смогут продолжить
загрузку с того места, на котором произошёл обрыв.</p>
</div>

<p>Вместо этого используйте, пожалуйста, инструменты, поддерживающие
докачку, обычно они называются <q>менеджерами загрузки</q>. Существует
множество дополнений для браузеров, которые решают эту задачу, либо вы
можете установить отдельную программу. Например, вы можете использовать
<a href="https://aria2.github.io/">aria2</a> (Linux, Win), <a
href="https://www.downthemall.net/">DownThemAll</a> (дополнение для браузера) или (из
командной строки) <q><tt>wget&nbsp;-c&nbsp;</tt><em>URL</em></q>
или <q><tt>curl&nbsp;-C&nbsp;-&nbsp;-L&nbsp;-O&nbsp;</tt><em>URL</em></q>.
Вы также можете посмотреть <a
href="https://en.wikipedia.org/wiki/Comparison_of_download_managers">сравнение
менеджеров загрузки</a>

<p>Для загрузки доступны следующие образы Debian:</p>

<ul>

  <li><a href="#stable">Официальные образы CD/DVD <q>стабильного</q> выпуска</a></li>

  <li><a href="https://cdimage.debian.org/cdimage/weekly-builds/">Официальные
  образы CD/DVD <q>тестируемого</q> дистрибутива (<em>генерируются
  еженедельно</em>)</a></li>

</ul>

<p>Смотрите также:</p>
<ul>

  <li>Полный <a href="#mirrors">список зеркал <tt>debian-cd/</tt></a></li>

  <li>Образы <q>network install</q>
можно найти на странице <a href="../netinst/">network install</a>.</li>

  <li>Образы <q>netinst</q> для тестируемого выпуска
  формируются ежедневно и известны как рабочие срезы. Их можно найти на странице
  <a href="$(DEVEL)/debian-installer/">Debian-Installer</a>.</li>

</ul>

<hr />

<h2><a name="stable">Официальные образы CD/DVD <q>стабильного</q> выпуска</a></h2>

<p>Для установки Debian на машины без соединения c Internet можно использовать
образы CD (700&nbsp;МБ каждый) или образы DVD(4.7&nbsp;ГБ каждый).
Загрузите файл образа первого CD или DVD, запишите его с помощью какой-нибудь программы для записи CD/DVD (образы
для архитектур i386 и amd64 можно записать на USB-накопитель), и затем загрузитесь с этих дисков.</p>

<p><strong>Первый</strong> CD/DVD диск содержит все файлы, необходимые для
установки стандартной системы Debian.<br />
Для избежания бесполезных загрузок <strong>не</strong> закачивайте
файлы других образов CD или DVD, если вы не знаете, нужны ли вам находящиеся
на них пакеты.</p>

<div class="line">
<div class="item col50">
<p><strong>CD</strong></p>

<p>Следующие ссылки указывают на файлы образов размером до 700&nbsp;МБ,
что делает их доступными для записи на обычные диски CD-R(W):</p>

<stable-full-cd-images />
</div>
<div class="item col50 lastcol">
<p><strong>DVD</strong></p>

<p>Следующие ссылки указывают на файлы образов размером до 4.7&nbsp;ГБ,
что делает их доступными для записи на обычные диски DVD-R/DVD+R и подобные
носители:</p>

<stable-full-dvd-images />
</div><div class="clear"></div>
</div>

<p>Обязательно просмотрите документацию перед установкой.
<strong>Если вы хотите быстрее начать установку</strong>, прочитайте наше
<a href="$(HOME)/releases/stable/amd64/apa">Howto по установке</a>, быстрое
введение в процесс установки. Другая полезная информация:
</p>
<ul>
<li><a href="$(HOME)/releases/stable/installmanual">Руководство по установке</a>,
    детальные инструкции по установке</li>
<li><a href="https://wiki.debian.org/DebianInstaller">Документация по
    Debian-Installer</a>, включает в себя FAQ с общими вопросами и ответами</li>
<li><a href="$(HOME)/releases/stable/debian-installer/#errata">Debian-Installer
    Errata</a>, список известных проблем в программе установки</li>
</ul>

<hr />

<h2><a name="mirrors">Зарегистрированные зеркала архива <q>debian-cd</q></a></h2>

<p>Имейте в виду, что <strong>некоторые зеркала обновляются недостаточно часто</strong> &mdash;
текущая версия образов <q>стабильного выпуска</q> &mdash; <strong><current-cd-release></strong>.

<p><strong>Если вы не уверены, используйте <a href="https://cdimage.debian.org/debian-cd/">главный
сервер образов в Швеции</a></strong>.</p>

<p>Вы заинтересованы в публикации образов CD Debian на
вашем зеркале? Если да, см. <a href="../mirroring/">инструкции по
настройке зеркала образов компакт-дисков</a>.</p>

#use wml::debian::countries
#include "$(ENGLISHDIR)/CD/http-ftp/cdimage_mirrors.list"
