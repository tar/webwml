msgid ""
msgstr ""
"Project-Id-Version: fa\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: \n"
"Last-Translator: Seyed mohammad ali Hosseinifard <ali_hosseine@yahoo.com>\n"
"Language-Team: Debian-l10n-persian <Debian-l10n-persian@lists.debian.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-Language: Persian\n"
"X-Poedit-Country: IRAN, ISLAMIC REPUBLIC OF\n"
"X-Poedit-SourceCharset: utf-8\n"

#: ../../english/search.xml.in:7
msgid "Debian website"
msgstr "وبسایت دبیان"

#: ../../english/search.xml.in:9
msgid "Search the Debian website."
msgstr "جستجوی وبسایت دبیان"

#: ../../english/template/debian/basic.wml:19
#: ../../english/template/debian/navbar.wml:11
msgid "Debian"
msgstr "دبیان"

#: ../../english/template/debian/basic.wml:48
msgid "Debian website search"
msgstr "جستجوی وبسایت دبیان"

#: ../../english/template/debian/common_translation.wml:4
msgid "Yes"
msgstr "بله"

#: ../../english/template/debian/common_translation.wml:7
msgid "No"
msgstr "خیر"

#: ../../english/template/debian/common_translation.wml:10
msgid "Debian Project"
msgstr "پروژه دبیان"

#: ../../english/template/debian/common_translation.wml:13
msgid ""
"Debian is an operating system and a distribution of Free Software. It is "
"maintained and updated through the work of many users who volunteer their "
"time and effort."
msgstr ""
"دبیان یک سیستم‌عامل و توزیع آزاد است. این توزیع با تلاش و زمان داوطلبان "
"نگهداری و به‌روز می‌شود."

#: ../../english/template/debian/common_translation.wml:16
msgid "debian, GNU, linux, unix, open source, free, DFSG"
msgstr "دبیان، گنو، لینوکس، یونیکس، منبع باز، آزاد، DFSG"

#: ../../english/template/debian/common_translation.wml:19
msgid "Back to the <a href=\"m4_HOME/\">Debian Project homepage</a>."
msgstr "بازگشت به <a href=\"m4_HOME/\">صفحه نخست پروژه دبیان</a>."

#: ../../english/template/debian/common_translation.wml:22
#: ../../english/template/debian/links.tags.wml:149
msgid "Home"
msgstr "خانه"

#: ../../english/template/debian/common_translation.wml:25
msgid "Skip Quicknav"
msgstr "رد شدن از Quicknav"

#: ../../english/template/debian/common_translation.wml:28
msgid "About"
msgstr "درباره"

#: ../../english/template/debian/common_translation.wml:31
msgid "About Debian"
msgstr "درباره دبیان"

#: ../../english/template/debian/common_translation.wml:34
msgid "Contact Us"
msgstr "تماس با ما"

#: ../../english/template/debian/common_translation.wml:37
msgid "Legal Info"
msgstr "اطلاعات حقوقی"

#: ../../english/template/debian/common_translation.wml:40
msgid "Data Privacy"
msgstr "حریم خصوصی داده‌ها"

#: ../../english/template/debian/common_translation.wml:43
msgid "Donations"
msgstr "کمک‌های مالی"

#: ../../english/template/debian/common_translation.wml:46
msgid "Events"
msgstr "رویدادها"

#: ../../english/template/debian/common_translation.wml:49
msgid "News"
msgstr "اخبار"

#: ../../english/template/debian/common_translation.wml:52
msgid "Distribution"
msgstr "توزیع"

#: ../../english/template/debian/common_translation.wml:55
msgid "Support"
msgstr "پشتیبانی"

#: ../../english/template/debian/common_translation.wml:58
msgid "Pure Blends"
msgstr "ترکیبات منحصر به فرد"

#: ../../english/template/debian/common_translation.wml:61
#: ../../english/template/debian/links.tags.wml:46
msgid "Developers' Corner"
msgstr "کُنج توسعه‌دهندگان"

#: ../../english/template/debian/common_translation.wml:64
msgid "Documentation"
msgstr "مستندات"

#: ../../english/template/debian/common_translation.wml:67
msgid "Security Information"
msgstr "اطلاعات امنیتی"

#: ../../english/template/debian/common_translation.wml:70
msgid "Search"
msgstr "جستجو"

#: ../../english/template/debian/common_translation.wml:73
msgid "none"
msgstr "هیچ‌کدام"

#: ../../english/template/debian/common_translation.wml:76
msgid "Go"
msgstr "برو"

#: ../../english/template/debian/common_translation.wml:79
msgid "worldwide"
msgstr "جهانی"

#: ../../english/template/debian/common_translation.wml:82
msgid "Site map"
msgstr "نقشه سایت"

#: ../../english/template/debian/common_translation.wml:85
msgid "Miscellaneous"
msgstr "گوناگون"

#: ../../english/template/debian/common_translation.wml:88
#: ../../english/template/debian/links.tags.wml:104
msgid "Getting Debian"
msgstr "دریافت دبیان"

#: ../../english/template/debian/common_translation.wml:91
msgid "The Debian Blog"
msgstr "بلاگ دبیان"

#: ../../english/template/debian/common_translation.wml:94
msgid "Debian Micronews"
msgstr "اخبار کوتاه دبیان"

#: ../../english/template/debian/common_translation.wml:97
msgid "Debian Planet"
msgstr "سیاره دبیان"

#: ../../english/template/debian/common_translation.wml:100
msgid "Last Updated"
msgstr "آخرین به‌روزرسانی"

#: ../../english/template/debian/ddp.wml:6
msgid ""
"Please send all comments, criticisms and suggestions about these web pages "
"to our <a href=\"mailto:debian-doc@lists.debian.org\">mailing list</a>."
msgstr ""
"لطفاً همه نظرات، انتقادات و پیشنهادات خود پیرامون این صفحات وب را به <a href="
"\"mailto:debian-doc@lists.debian.org\">فهرست پستی</a> ما ارسال کنید."

#: ../../english/template/debian/fixes_link.wml:11
msgid "not needed"
msgstr "نیاز نیست"

#: ../../english/template/debian/fixes_link.wml:14
msgid "not available"
msgstr "موجود نیست"

#: ../../english/template/debian/fixes_link.wml:17
msgid "N/A"
msgstr "N/A"

#: ../../english/template/debian/fixes_link.wml:20
msgid "in release 1.1"
msgstr "در انتشار ۱٫۱"

#: ../../english/template/debian/fixes_link.wml:23
msgid "in release 1.3"
msgstr "در انتشار ۱٫۳"

#: ../../english/template/debian/fixes_link.wml:26
msgid "in release 2.0"
msgstr "در انتشار ۲٫۰"

#: ../../english/template/debian/fixes_link.wml:29
msgid "in release 2.1"
msgstr "در انتشار ۲٫۱"

#: ../../english/template/debian/fixes_link.wml:32
msgid "in release 2.2"
msgstr "در انتشار ۲٫۲"

#: ../../english/template/debian/footer.wml:84
msgid ""
"See our <a href=\"m4_HOME/contact\">contact page</a> to get in touch. Web "
"site source code is <a href=\"https://salsa.debian.org/webmaster-team/webwml"
"\">available</a>."
msgstr ""

#: ../../english/template/debian/footer.wml:87
msgid "Last Modified"
msgstr "آخرین تغییر"

#: ../../english/template/debian/footer.wml:90
msgid "Last Built"
msgstr "آخرین ساخت"

#: ../../english/template/debian/footer.wml:93
msgid "Copyright"
msgstr "حق نشر"

#: ../../english/template/debian/footer.wml:96
msgid "<a href=\"https://www.spi-inc.org/\">SPI</a> and others;"
msgstr "<a href=\"https://www.spi-inc.org/\">SPI</a> و دیگران;"

#: ../../english/template/debian/footer.wml:99
msgid "See <a href=\"m4_HOME/license\" rel=\"copyright\">license terms</a>"
msgstr "<a href=\"m4_HOME/license\" rel=\"copyright\">شرایط مجوز</a> را ببینید"

#: ../../english/template/debian/footer.wml:102
msgid ""
"Debian is a registered <a href=\"m4_HOME/trademark\">trademark</a> of "
"Software in the Public Interest, Inc."
msgstr ""
"دبیان یک <a href=\"m4_HOME/trademark\">نشان تجاری</a> ثبت شده متعلق به "
"Software in the Public Interest, Inc است."

#: ../../english/template/debian/languages.wml:196
#: ../../english/template/debian/languages.wml:232
msgid "This page is also available in the following languages:"
msgstr "این صفحه به زبان‌های زیر نیز در دسترس است:"

#: ../../english/template/debian/languages.wml:265
msgid "How to set <a href=m4_HOME/intro/cn>the default document language</a>"
msgstr "نحوه تنظیم <a href=m4_HOME/intro/cn>زبن پیش‌فرض سند</a>"

#: ../../english/template/debian/languages.wml:323
msgid "Browser default"
msgstr ""

#: ../../english/template/debian/languages.wml:323
msgid "Unset the language override cookie"
msgstr ""

#: ../../english/template/debian/links.tags.wml:4
msgid "Debian International"
msgstr "دبیان بین‌المللی"

#: ../../english/template/debian/links.tags.wml:7
msgid "Partners"
msgstr "همکاران"

#: ../../english/template/debian/links.tags.wml:10
msgid "Debian Weekly News"
msgstr "اخبار هفتگی دبیان"

#: ../../english/template/debian/links.tags.wml:13
msgid "Weekly News"
msgstr "اخبار هفتگی"

#: ../../english/template/debian/links.tags.wml:16
msgid "Debian Project News"
msgstr "اخبار پروژه دبیان"

#: ../../english/template/debian/links.tags.wml:19
msgid "Project News"
msgstr "اخبار پروژه"

#: ../../english/template/debian/links.tags.wml:22
msgid "Release Info"
msgstr "اطلاعات انتشار"

#: ../../english/template/debian/links.tags.wml:25
msgid "Debian Packages"
msgstr "بسته‌های دبیان"

#: ../../english/template/debian/links.tags.wml:28
msgid "Download"
msgstr "بارگیری"

#: ../../english/template/debian/links.tags.wml:31
msgid "Debian&nbsp;on&nbsp;CD"
msgstr "Debian&nbsp;on&nbsp;CD"

#: ../../english/template/debian/links.tags.wml:34
msgid "Debian Books"
msgstr "کتاب‌های دبیان"

#: ../../english/template/debian/links.tags.wml:37
msgid "Debian Wiki"
msgstr "ویکی دبیان"

#: ../../english/template/debian/links.tags.wml:40
msgid "Mailing List Archives"
msgstr "بایگانی فهرست‌های پستی"

#: ../../english/template/debian/links.tags.wml:43
msgid "Mailing Lists"
msgstr "فهرست‌های پستی"

#: ../../english/template/debian/links.tags.wml:49
msgid "Social Contract"
msgstr "قرارداد اجتماعی"

#: ../../english/template/debian/links.tags.wml:52
msgid "Code of Conduct"
msgstr "مرام‌نامه"

#: ../../english/template/debian/links.tags.wml:55
msgid "Debian 5.0 - The universal operating system"
msgstr "دبیان ۵٫۰ - سیستم‌عامل همگانی"

#: ../../english/template/debian/links.tags.wml:58
msgid "Site map for Debian web pages"
msgstr "نقشه سایت صفحات وب دبیان"

#: ../../english/template/debian/links.tags.wml:61
msgid "Developer Database"
msgstr "پایگاه‌داده توسعه‌دهندگان"

#: ../../english/template/debian/links.tags.wml:64
msgid "Debian FAQ"
msgstr "پرسش‌های متداول پیرامون دبیان"

#: ../../english/template/debian/links.tags.wml:67
msgid "Debian Policy Manual"
msgstr "راهنمای خط مشی دبیان"

#: ../../english/template/debian/links.tags.wml:70
msgid "Developers' Reference"
msgstr "مرجع توسعه‌دهندگان"

#: ../../english/template/debian/links.tags.wml:73
msgid "New Maintainers' Guide"
msgstr "راهنمای نگه‌دارندگان تازه‌وارد"

#: ../../english/template/debian/links.tags.wml:76
msgid "Release Critical Bugs"
msgstr "مشکلات بحرانی انتشار"

#: ../../english/template/debian/links.tags.wml:79
msgid "Lintian Reports"
msgstr "گزارش‌های Lintian"

#: ../../english/template/debian/links.tags.wml:83
msgid "Archives for users' mailing lists"
msgstr "بایگانی فهرست‌های پستی کاربران"

#: ../../english/template/debian/links.tags.wml:86
msgid "Archives for developers' mailing lists"
msgstr "بایگانی فهرست‌های پستی توسعه‌دهندگان"

#: ../../english/template/debian/links.tags.wml:89
msgid "Archives for i18n/l10n mailing lists"
msgstr "بایگانی فهرست‌های پستی محلی‌سازی"

#: ../../english/template/debian/links.tags.wml:92
msgid "Archives for ports' mailing lists"
msgstr "بایگانی فهرست‌های پستی پورت‌ها"

#: ../../english/template/debian/links.tags.wml:95
msgid "Archives for mailing lists of the Bug tracking system"
msgstr "بایگانی فهرست‌های پستی مربوط به سامانه ردیابی اشکال"

#: ../../english/template/debian/links.tags.wml:98
msgid "Archives for miscellaneous mailing lists"
msgstr "بایگانی فهرست‌های پستی گوناگون"

#: ../../english/template/debian/links.tags.wml:101
msgid "Free Software"
msgstr "نرم‌افزار آزاد"

#: ../../english/template/debian/links.tags.wml:107
msgid "Development"
msgstr "توسعه"

#: ../../english/template/debian/links.tags.wml:110
msgid "Help Debian"
msgstr "کمک به دبیان"

#: ../../english/template/debian/links.tags.wml:113
msgid "Bug reports"
msgstr "گزارش اشکال"

#: ../../english/template/debian/links.tags.wml:116
msgid "Ports/Architectures"
msgstr "پورت‌ها/معماری‌ها"

#: ../../english/template/debian/links.tags.wml:119
msgid "Installation manual"
msgstr "راهنمای نصب"

#: ../../english/template/debian/links.tags.wml:122
msgid "CD vendors"
msgstr "فروشندگان سی‌دی"

#: ../../english/template/debian/links.tags.wml:125
msgid "CD/USB ISO images"
msgstr "تصاویر ISO سی‌دی/یواس‌بی"

#: ../../english/template/debian/links.tags.wml:128
msgid "Network install"
msgstr "نصب ازطریق شبکه"

#: ../../english/template/debian/links.tags.wml:131
msgid "Pre-installed"
msgstr "از پیش نصب شده"

#: ../../english/template/debian/links.tags.wml:134
msgid "Debian-Edu project"
msgstr "پروژه Debian-Edu"

#: ../../english/template/debian/links.tags.wml:137
msgid "Salsa &ndash; Debian Gitlab"
msgstr "Salsa &ndash; گیت‌لب دبیان"

#: ../../english/template/debian/links.tags.wml:140
msgid "Quality Assurance"
msgstr "تضمین کیفیت"

#: ../../english/template/debian/links.tags.wml:143
msgid "Package Tracking System"
msgstr "سیستم ردیابی بسته"

#: ../../english/template/debian/links.tags.wml:146
msgid "Debian Developer's Packages Overview"
msgstr "مروری بر بسته‌های توسعه‌دهندگان دبیان"

#: ../../english/template/debian/navbar.wml:10
msgid "Debian Home"
msgstr "خانه دبیان"

#: ../../english/template/debian/navbar.wml:31
msgid "Bits from Debian"
msgstr "بیت‌هایی از دبیان"

#: ../../english/template/debian/navbar.wml:31
msgid "Blog"
msgstr "وبلاگ"

#: ../../english/template/debian/navbar.wml:32
msgid "Micronews"
msgstr "اخبار کوتاه"

#: ../../english/template/debian/navbar.wml:32
msgid "Micronews from Debian"
msgstr "اخبار کوتاه از دبیان"

#: ../../english/template/debian/navbar.wml:33
msgid "Planet"
msgstr "سیاره"

#: ../../english/template/debian/navbar.wml:33
msgid "The Planet of Debian"
msgstr "سیاره‌ی دبیان"

#: ../../english/template/debian/recent_list.wml:7
msgid "No items for this year."
msgstr "هیچ موردی برای این سال وجود ندارد"

#: ../../english/template/debian/recent_list.wml:11
msgid "proposed"
msgstr "پیشنهاد شده"

#: ../../english/template/debian/recent_list.wml:15
msgid "in discussion"
msgstr "در گفتگو"

#: ../../english/template/debian/recent_list.wml:19
msgid "voting open"
msgstr "رای گیری باز است"

#: ../../english/template/debian/recent_list.wml:23
msgid "finished"
msgstr "تمام شده"

#: ../../english/template/debian/recent_list.wml:26
msgid "withdrawn"
msgstr ""

#: ../../english/template/debian/recent_list.wml:30
msgid "Future events"
msgstr "رویدادهای آینده"

#: ../../english/template/debian/recent_list.wml:33
msgid "Past events"
msgstr "رویدادهای گذشته"

#: ../../english/template/debian/recent_list.wml:37
msgid "(new revision)"
msgstr "(بازبینی جدید)"

#: ../../english/template/debian/recent_list.wml:329
msgid "Report"
msgstr "گزارش"

#: ../../english/template/debian/redirect.wml:6
msgid "Page redirected to <newpage/>"
msgstr "صفحه به <newpage/> هدایت شد"

#: ../../english/template/debian/redirect.wml:14
msgid ""
"This page has been renamed to <url <newpage/>>, please update your links."
msgstr ""
"این صفحه به <url <newpage/>> تغییر نام داده است، لطفاً پیوندهای خود را "
"به‌روزرسانی کنید."

#. given a manual name and an architecture, join them
#. if you need to reorder the two, use "%2$s ... %1$s", cf. printf(3)
#: ../../english/template/debian/release.wml:7
msgid "<void id=\"doc_for_arch\" />%s for %s"
msgstr "<void id=\"doc_for_arch\" />%s for %s"

#: ../../english/template/debian/translation-check.wml:37
msgid ""
"<em>Note:</em> The <a href=\"$link\">original document</a> is newer than "
"this translation."
msgstr "<em>توجه:</em> <a href=\"$link\">سند اصلی</a> از این ترجمه جدیدتر است."

#: ../../english/template/debian/translation-check.wml:43
msgid ""
"Warning! This translation is too out of date, please see the <a href=\"$link"
"\">original</a>."
msgstr ""
"هشدار! این ترجمه خیلی قدیمی است، لطفاً <a href=\"$link\">نسخه اصلی</a> را "
"ببینید."

#: ../../english/template/debian/translation-check.wml:49
msgid ""
"<em>Note:</em> The original document of this translation no longer exists."
msgstr "<em>توجه:</em> سند اصلی این ترجمه دیگر در دسترس نیست."

#: ../../english/template/debian/translation-check.wml:56
msgid "Wrong translation version!"
msgstr "نسخه ترجمه اشتباه است!"

#: ../../english/template/debian/url.wml:4
msgid "URL"
msgstr "URL"

#: ../../english/template/debian/users.wml:12
msgid "Back to the <a href=\"../\">Who's using Debian? page</a>."
msgstr "بازگشت به <a href=\"../\">صفحه چه کسی از دبیان استفاده می‌کند؟</a>."

#~ msgid ""
#~ "To report a problem with the web site, please e-mail our publicly "
#~ "archived mailing list <a href=\"mailto:debian-www@lists.debian.org"
#~ "\">debian-www@lists.debian.org</a> in English.  For other contact "
#~ "information, see the Debian <a href=\"m4_HOME/contact\">contact page</a>. "
#~ "Web site source code is <a href=\"https://salsa.debian.org/webmaster-team/"
#~ "webwml\">available</a>."
#~ msgstr ""
#~ "برای گزارش مشکل در وبسایت، لطفاً به زبان انگلیسی به فهرست پستی ما که بصورت "
#~ "عمومی بایگانی می‌شود ایمیل بزنید <a href=\"mailto:debian-www@lists.debian."
#~ "org\">debian-www@lists.debian.org</a>. همچنین برای ارتباط با تیم محلی‌سازی "
#~ "فارسی دبیان، می‌توانید به <a href=\"mailto:debian-l10n-persian@lists."
#~ "debian.org\">debian-l10n-persian@lists.debian.org</a> ایمیل بزنید. برای "
#~ "اطلاعات تماس بیشتر، <a href=\"m4_HOME/contact\">صفحه تماس </a> دبیان را "
#~ "ببینید. کد منبع وبسایت نیز <a href=\"https://salsa.debian.org/webmaster-"
#~ "team/webwml\">در دسترس</a> است."
