#use wml::debian::template title="Debian telepítése az Interneten keresztül" BARETITLE=true
#use wml::debian::toc
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/images.data"
#use wml::debian::translation-check translation="457bcb4118a5a79a3dc0e676d42418e45a8d65f3" maintainer="Szabolcs Siebenhofer"

<p>A Debian telepítésének e módszeréhez működő internet kapcsolatra van szükség a
telepítés <em>közben</em>. Összehasonlításul, más módszerekhez képest, kevesebb adat 
kerül letöltésre, ha ugyan azok a követelményeid. Az ethernet és a vezeték nélküli
hálózati kapcsolatok is támogatottak. Belső ISDN kártyákhoz sajnos <em>nincs</em>
támogatás.</p>
<p>Három féle lehetőség van a hálózaton keresztüli telepítésre</p>

<toc-display />
<div class="line">
<div class="item col50">

<toc-add-entry name="smallcd">Kis CD-k vagy USB pendrive-ok</toc-add-entry>

<p>A következőkben képfájlokat találsz. Lejebb kiválaszthatod a 
megfelelő processzor architektúrát.</p>

<stable-netinst-images />
</div>
<div class="clear"></div>
</div>

<p>A részleteket megtalálod itt: <a href="../CD/netinst/">Hálózati telepítés minimal CD-ről</a></p>

<div class="line">
<div class="item col50">

<toc-add-entry name="verysmall">Apró CD-k, rugalmas USB pendrivok, stb.</toc-add-entry>

<p>Többféle, kis méretű CD képet tölthetsz le, ami USB pendrive-okhoz vagy hasonlókhoz 
használható. A képet ki kell írni az adott médiára, majd az azzal történő rendszerbetöltés után 
már kezdődhet is a telepítés.</p>

<p>A nagyon kis képekről történő telepítésnél tapasztalható néhány különgség a 
különböző architektúrák támogatásánál.
</p>

<p>A részleteket lásd az 
<a href="$(HOME)/releases/stable/installmanual">architektúrádhoz tartozó
telepítési leírásban</a>, különösen az <q>Elérhető rendszer 
telepítő médiák</q> fejezetben.</p>

<p>
Itt megtalálhatod az elérhető képfájlok linkjeinek listáját (további információt
a MANIFEST fájlban találsz):
</p>

<stable-verysmall-images />
</div>
<div class="item col50 lastcol">

<toc-add-entry name="netboot">Hálózati rendszerbetöltés</toc-add-entry>

<p>Beállíthatsz TFTP vagy DHCP (vagy BOOTP vagy RARP) szervert, ami telepítési
médiaként fog szolgálni a helyi hálózaton lévő gépeknek. Ha a munkaállomások BIOS-a
támogatja, a Debian telepítőt a hálózaton keresztül is betöltheted (PXE vagy TFTP 
használatával) és a Debian többi része a hálózaton keresztül települ.</p>

<p>Nem minden számítógép támogatja a hálózaton keresztüli rendszerbetöltést. 
Mivel további munkát igényel, ezt a módszert nem ajánljuk kezdő felhasználók 
részére.</p>

<p>A részleteket lásd az 
<a href="$(HOME)/releases/stable/installmanual">architektúrádhoz tartozó
telepítési leírásban</a>, különösen a
<q>Fájlok előkészítése TFTP-n keresztüli hálózati rendszerbetöltéshez</q> fejezetben.</p>
<p>Itt megtalálhatod az elérhető képfájlok linkjeinek listáját (további információt
a MANIFEST fájlban találsz):</p>

<stable-netboot-images />
 </div>
 </div>
